#!/usr/bin/perl

# Memory configurator. 
# Designed to be architecture- and distribution independent.
#
# Copyright (C) 2000 Helix Code, Inc.
#
# Authors: Bradford Hovinen <hovinen@helixcode.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Library General Public License as published
# by the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

# Best viewed with 100 columns of width.

# Configuration files affected:
#
# /etc/fstab

# Running programs affected/used:
#
# /sbin/swapon /sbin/swapoff /sbin/mkswap


# --- Usage text --- #

my $Usage =<<"End_of_Usage;";
Usage: memory-conf <--get | --set | --filter | --help>
                 [--prefix <location>] [--disable-immediate]
                 [--verbose]

       Major operations (specify one of these):

       -g --get      Prints the current memory configuration to standard
                     output, as as standalone XML document. The parameters
                     are read from the host\'s low-level configuration.

       -s --set      Updates the system memory configuration from a standalone
                     XML document read from standard input. The format is
                     the same as for the document generated with --get.

       -f --filter   Reads XML configuration from standard input, parses it,
                     and writes the configurator\'s impression of it back to
                     standard output. Good for debugging and parsing tests.

       -h --help     Prints this page to standard output.

          --version  Prints version information to standard output.

       Modifiers (specify any combination of these):

       -p --prefix <location>  Specifies a directory prefix where the
                     configuration is looked for or stored. When storing
                     (with --set), directories and files may be created.

          --disable-immediate  With --set, prevents the configurator from
                     running any commands that make immediate changes to
                     the system configuration. Use with --prefix to make a
                     dry run that won\'t affect your configuration.

                     With --get, suppresses running of non-vital external
                     programs that might take a long time to finish.

       -v --verbose  Turns on diagnostic messages to standard error.

End_of_Usage;

$version = "0.1.0";

# --- System config file locations --- #

# We list each config file type with as many alternate locations as possible.
# They are tried in array order. First found = used.

@fstab_names =       ( "/etc/fstab" );

# --- Internal configuration variables --- #

# Configuration is parsed/read to, and printed/written from, these temporary variables.

@cf_swapinfo = ();

# --- Operation modifying variables --- #

# Variables are set to their default value, which may be overridden by user. Note
# that a $prefix of "" will cause the configurator to use '/' as the base path,
# and disables creation of directories and writing of previously non-existent
# files.

$prefix = "";
$verbose = 0;
$do_immediate = 1;


# --- XML print formatting  --- #

# enter: Call after entering a block. Increases indent level.
# leave: Call before leaving a block. Decreases indent level.
# indent: Call before printing a line. Indents to current level. 
# vspace: Ensures there is a vertical space of one and only one line.

$indent_level = 0;
$have_vspace = 0;

sub enter  { $indent_level += 2; }
sub leave  { $indent_level -= 2; }
sub indent { for ($i = 0; $i < $indent_level; $i++) { print " "; } $have_vspace = 0; }
sub vspace { if (not $have_vspace) { print "\n"; $have_vspace = 1; } }


# --- XML scanning --- #

# This code tries to replace XML::Parser scanning from stdin in tree mode.


@xml_scan_list;


sub xml_scan_make_kid_array
  {
    my %hash = {};
    my @sublist;
    
    @attr = @_[0] =~ /[^\t\n\r ]+[\t\n\r ]*([a-zA-Z]+)[ \t\n\r]*\=[ \t\n\r\"\']*([a-zA-Z]+)/g;
    %hash = @attr;
    
    push(@sublist, \%hash);
    return(\@sublist);
  }


sub xml_scan_recurse;

sub xml_scan_recurse
  {
    my @list;
    if (@_) { @list = @_[0]->[0]; }
    
    while (@xml_scan_list)
      {
	$el = @xml_scan_list[0]; shift @xml_scan_list;
	
	if ((not $el) || $el =~ /^\<[!?].*\>$/s) { next; } # Empty strings, PI and DTD must go.
	
	if ($el =~ /^\<.*\/\>$/s) # Empty.
	  {
	    $el =~ /^\<([a-zA-Z]+).*\/\>$/s;
	    push(@list, $1);
	    push(@list, xml_scan_make_kid_array($el));
	  }
	elsif ($el =~ /^\<\/.*\>$/s) # End.
	  {
	    last;
	  }
	elsif ($el =~ /^\<.*\>$/s) # Start.
	  {
	    $el =~ /^\<([a-zA-Z]+).*\>$/s;
	    push(@list, $1);
	    $sublist = xml_scan_make_kid_array($el);
	    push(@list, xml_scan_recurse($sublist));
	    next;
	  }
	elsif ($el ne "")	# PCDATA.
	  {
	    push(@list, 0);
	    push(@list, "$el");
	  }
      }
    
    return(\@list);
  }


sub xml_scan
  {
    my $doc; my @tree;
    read STDIN, $doc, 512000;	# Read in document (FIXME: ugly).
print STDERR $doc if $verbose;
    @xml_scan_list = ($doc =~ /([^\<]*)(\<[^\>]*\>)[ \t\n\r]*/mg); # pcdata, tag, pcdata, tag, ...
    
    $tree = xml_scan_recurse;
    
    return($tree);
    
    #  $" = "\n";
    #  print "@list\n";
  }


@xml_entities = ( "&lt;", '<', "&gt;", '>', "&apos;", '\'', "&quot;", '"' );

sub xml_entities_to_plain
  {
    my $in = @_[0];
    my $out = "";
    my @xe;
    
    $in = $$in;
    
    my @elist = ($in =~ /([^&]*)(\&[a-zA-Z]+\;)?/mg); # text, entity, text, entity, ...
    
    while (@elist)
      {
	# Join text.
	
	$out = join('', $out, @elist[0]);
	shift @elist;
	
	# Find entity and join its text equivalent.
	# Unknown entities are simply removed.
	
	for (@xe = @xml_entities; @xe; )
	  {
	    if (@xe[0] eq @elist[0]) { $out = join('', $out, @xe[1]); last; }
	    shift @xe; shift @xe;
	  }
	
	shift @elist;
      }
    
    return($out);
  }


sub xml_plain_to_entities
  {
    my $in = @_[0];
    my $out = "";
    my @xe;
    my $joined = 0;
    
    $in = $$in;
    
    my @clist = split(//, $in);
    
    while (@clist)
      {
	# Find character and join its entity equivalent.
	# If none found, simply join the character.
	
	$joined = 0;		# Cumbersome.
	
	for (@xe = @xml_entities; @xe && !$joined; )
	  {
	    if (@xe[1] eq @clist[0]) { $out = join('', $out, @xe[0]); $joined = 1; }
	    shift @xe; shift @xe;
	  }
	
	if (!$joined) { $out = join('', $out, @clist[0]); }
	shift @clist;
      }
    
    return($out);
  }


# --- String and array manipulation --- #

# Boolean/strings conversion.

sub read_boolean
  {
    if (@_[0] eq "true") { return(1); }
    elsif (@_[0] eq "yes") { return(1); }
    return(0);
  }

sub print_boolean_yesno
  {
    if (@_[0] == 1) { return("yes"); }
    return("no");
  }

sub print_boolean_truefalse
  {
    if (@_[0] == 1) { return("true"); }
    return("false");
  }


# Pushes a value to an array, only if it's not already in there.
# I'm sure there's a smarter way to do this. Should only be used for small lists,
# as it's O(N^2). Larger lists with unique members should use a hash.

sub push_unique
  {
    my $arr = @_[0];
    my $found;
    my $i;
    
    # Go through all elements in pushed list.
    
    for ($i = 1; @_[$i]; $i++)
      {
	# Compare against all elements in destination array.
	
	$found = "";
	for $elem (@$arr)
	  {
	    if ($elem eq @_[$i]) { $found = $elem; last; }
	  }
	
	if ($found eq "") { push(@$arr, @_[$i]); }
      }
  }


sub is_line_comment_start
  {
    if (@_[0] =~ /^\#/) { return(1); }
    return(0);
  }


# --- File operations --- #


sub open_read_from_names
  {
    local *FILE;
    my $fname = "";
    
    for $name (@_)
      {
	if (open(FILE, "$prefix/$name")) { $fname = $name; last; }
      }
    
    if ($verbose)
      {
	(my $fullname = "$prefix/$fname") =~ tr/\//\//s;  # '//' -> '/'	

	if ($fname ne "") 
	  { 
	    print STDERR "Reading options from \"$fullname\".\n"; 
	  }
	else 
	  { 
	    print STDERR "Could not read \[@_\].\n"; 
	  }
      }
    
    return *FILE;
  }


sub open_write_from_names
  {
    local *FILE;
    my $name;
    my $fullname;
    
    # Find out where it lives.
    
    for $elem (@_) { if (stat($elem) ne "") { $name = $elem; last; } }
    
    if ($name eq "")
      {
	# If we couldn't locate the file, and have no prefix, give up.
	
	# If we have a prefix, but couldn't locate the file relative to '/',
	# take the first name in the array and let that be created in $prefix.
	
	if ($prefix eq "")
	  {
	    if ($verbose) { print STDERR "No file to replace: \[@_\].\n"; }
	    return(0);
	  }
	else
	  {
	    $name = @_[0];
	    if ($verbose)
	      {
		(my $fullname = "$prefix/$name") =~ tr/\//\//s;
		print STDERR "Could not find \[@_\]. Writing to \"$fullname\".\n";
	      }
	  }
      }
    elsif ($verbose)
      {
	(my $fullname = "$prefix/$name") =~ tr/\//\//s;
	print STDERR "Found \"$name\". Writing to \"$fullname\".\n";
      }
    
    ($name = "$prefix/$name") =~ tr/\//\//s;  # '//' -> '/' 
      create_path($name);
    
    # Make a backup if the file already exists - if the user specified a prefix,
    # it might not.
    
    if (stat($name))
      {
	# NOTE: Might not work everywhere. Might be unsafe if the user is allowed
	# to specify a $name list somehow, in the future.
	
	system("cp $name $name.confsave >/dev/null 2>/dev/null");
      }
    
    # Truncate and return filehandle.
    
    if (!open(FILE, ">$name") && $verbose)
      {
	print STDERR "Error: Failed to write to \"$name\". Are you root?\n";
      }
    
    return *FILE;
  }


sub open_filter_write_from_names
  {
    local *INFILE;
    local *OUTFILE;
    my $name;
    
    # Find out where it lives.
    
    for $elem (@_) { if (stat($elem) ne "") { $name = $elem; last; } }
    
    if ($name eq "")
      {
	# If we couldn't locate the file, and have no prefix, give up.
	
	# If we have a prefix, but couldn't locate the file relative to '/',
	# take the first name in the array and let that be created in $prefix.
	
	if ($prefix eq "")
	  {
	    if ($verbose) { print STDERR "No file to patch: \[@_\].\n"; }
	    return(0, 0);
	  }
	else
	  {
	    $name = @_[0];
	    if ($verbose)
	      {
		(my $fullname = "$prefix/$name") =~ tr/\//\//s;
		print STDERR "Could not find \[@_\]. Patching \"$fullname\".\n";
	      }
	  }
      }
    elsif ($verbose)
      {
	(my $fullname = "$prefix/$name") =~ tr/\//\//s;
	print STDERR "Found \"$name\". Patching \"$fullname\".\n";
      }
    
    ($name = "$prefix/$name") =~ tr/\//\//s;  # '//' -> '/' 
      create_path($name);
    
    # Make a backup if the file already exists - if the user specified a prefix,
    # it might not.
    
    if (stat($name))
      {
	# NOTE: Might not work everywhere. Might be unsafe if the user is allowed
	# to specify a $name list somehow, in the future.
	
	system("cp $name $name.confsave >/dev/null 2>/dev/null");
      }
    
    # Return filehandles. Backup file is used as filter input. It might be
    # invalid, in which case the caller should just write to OUTFILE without
    # bothering with INFILE filtering.
    
    open(INFILE, "$name.confsave");
    
    if (!open(OUTFILE, ">$name") && $verbose)
      {
	print STDERR "Error: Failed to write to \"$name\". Are you root?\n";
      }
    
    return(*INFILE, *OUTFILE);
  }


sub create_path
  {
    my $path;
    
    $path = @_[0];
    my @pelem = split(/\//, $path); # 'a/b/c/d/' -> 'a', 'b', 'c', 'd', ''
    
    for ($path = ""; @pelem; shift @pelem)
      {
	if (@pelem[1] ne "")
	  {
	    $path = "$path@pelem[0]";
	    mkdir($path, 0770);
	    $path = "$path/";
	  }
      }
  }


# --- XML parsing --- #


# Scan XML from standard input to an internal tree.

sub xml_parse
  {
    # Scan XML to tree.
    
    $tree = xml_scan;
    
    # Walk the tree recursively and extract configuration parameters.
    # This is the top level - find and enter the "networking" tag.
    
    while (@$tree)
      {
	if (@$tree[0] eq "memory") { xml_parse_memory(@$tree[1]); }
	
	shift @$tree;
	shift @$tree;
      }
    
    return($tree);
  }


# <memory>...</memory>

sub xml_parse_memory
  {
    my $tree = @_[0];
    
    shift @$tree;		# Skip attributes.
    
    while (@$tree)
      {
	if (@$tree[0] eq "swapdev") 
	  { 
	    push @cf_swapinfo, xml_parse_swapinfo (@$tree[1]); 
	  }
	
	shift @$tree;
	shift @$tree;
      }
  }


# <swap-device>...</swap-device>

sub xml_parse_swapinfo
  {
    my $tree = @_[0];
    my $node;
    my $entry = {};
    
    shift @$tree;		# Skip attributes.
    
    while (@$tree)
      {
	if (@$tree[0] eq 'device') 
	  { 
	    $entry->{device} = xml_get_word (@$tree[1]);
	  }
	elsif (@$tree[0] eq 'enabled') 
	  {
	    $node = @$tree[1];
	    $entry->{enabled} = read_boolean (@$node[0]->{state});
	  }
	elsif (@$tree[0] eq 'priority') 
	  { 
	    $entry->{priority} = xml_get_word (@$tree[1]);
	  }
	elsif (@$tree[0] eq 'size') 
	  { 
	    $entry->{size} = xml_get_word (@$tree[1]);
	  }
	elsif (@$tree[0] eq 'isfile') 
	  { 
	    $node = @$tree[1];
	    $entry->{is_file} = read_boolean (@$node[0]->{state});
	  }
	elsif (@$tree[0] eq 'isnew') 
	  { 
	    $node = @$tree[1];
	    $entry->{is_new} = read_boolean (@$node[0]->{state});
	  }
	
	shift @$tree;
	shift @$tree;
      }

    return $entry;
  }

# Compresses node into a word and returns it.

sub xml_get_word
  {
    my $tree = @_[0];
    
    shift @$tree;		# Skip attributes.
    
    while (@$tree)
      {
	if (@$tree[0] == 0)
	  {
	    my $retval;
	    
	    ($retval = @$tree[1]) =~ tr/ \n\r\t\f//d;
	    $retval = xml_entities_to_plain(\$retval);
	    return($retval);
	  }
	
	shift @$tree;
	shift @$tree;
      }
    
    return("");
  }


# Replaces misc. whitespace with spaces and returns text.

sub xml_get_text
  {
    my $tree = @_[0];
    
    shift @$tree;		# Skip attributes.
    
    while (@$tree)
      {
	if (@$tree[0] = 0)
	  {
	    ($retval = @$tree[1]) =~ tr/\n\r\t\f/    /;
	    $retval = xml_entities_to_plain(\$retval);
	    return($retval);
	  }
	
	shift @$tree;
	shift @$tree;
      }
  }


# --- XML printing --- #


sub xml_print
  {
    print "<?xml version='1.0' encoding='ISO-8859-1' standalone='yes'?>\n";
    print "<!DOCTYPE memory []>\n\n";
    print "<memory>\n";
    enter;

    vspace;
    indent; print "<!-- Configuration starts here -->\n";
    vspace;

    foreach $entry (@cf_swapinfo) {
      indent; print "<swapdev>\n";
      indent; indent; print "<device>$entry->{device}</device>\n";
      indent; indent; print "<enabled state='", 
                            print_boolean_yesno ($entry->{enabled}), 
                            "'/>\n";
      indent; indent; print "<priority>$entry->{priority}</priority>\n";
      indent; print "</swapdev>\n\n";
    }

    indent; print "<!-- End of configuration -->\n";
    vspace;

    leave;
    print "</memory>\n";
  }


# --- Get (read) config --- #

sub get_swap_entries
  {
    local *FSTAB_FILE;

    *FSTAB_FILE = open_read_from_names (@fstab_names);

    if (not FSTAB_FILE) 
      {
	print STDERR "Could not find file.\n";
	return; 
      }

    while (<FSTAB_FILE>)
      {
	my ($device, $mount_pt, $type, $options, $fs_freq, $fs_passno) = split;
	my (@option_list) = split /\,/, $options;
	my ($priority) = -1;
	my ($enabled);

	next if $type ne 'swap';

	if ($verbose) { print STDERR "Found swap entry:\n$_"; }

	if ($device =~ /^#(.*)/)
	  {
	    $device = $1;
	    $enabled = 0;
	  }
	else 
	  {
	    $enabled = 1;
	  }

	foreach $item (@option_list)
	  {
	    my ($key, $value) = split /\=/, $item;

	    $priority = $value if $key eq 'pri';
	  }

	push @cf_swapinfo, { 
			    "device" => $device, 
			    "enabled" => $enabled, 
			    "priority" => $priority
			   };
      }
  }


sub get
  {
    if ($verbose) 
      { 
	print STDERR "Getting system configuration, generating XML output.\n";
      }
    
    if ($verbose) { print STDERR "Getting swap entries.\n"; }
    
    get_swap_entries;
    
    if ($verbose) { print STDERR "Printing XML.\n"; }
    xml_print;
  }


# --- Set (write) config --- #

sub setup_swap_files
  {
    foreach $entry (@cf_swapinfo)
      {
	if ($entry->{is_new})
	  {
	    if ($entry->{is_file} && !-f $entry->{device})
	      {
		system ("/bin/dd if=/dev/zero of=$entry->{device} bs=1024 count=$entry->{size}");
	      }

	    system ("/sbin/mkswap $entry->{device}");
	  }

	if ($entry->{priority} == -1)
	  {
	    $priority = "";
	  }
	else 
	  {
	    $priority = "-p $entry->{priority}";
	  }

	if ($entry->{enabled}) 
	  {
	    system ("/sbin/swapon $entry->{device} $priority");
	  }
	else
	  {
	    system ("/sbin/swapoff $entry->{device}");
	  }
      }
  }

sub set_swap_entries
  {
    local (*FSTAB_IN_FILE, *FSTAB_OUT_FILE);
    my (@lines);
    my ($priority_str);

    (*FSTAB_IN_FILE, *FSTAB_OUT_FILE) = 
      open_filter_write_from_names (@fstab_names);

    while (<FSTAB_IN_FILE>)
      {
	my ($device, $mount_pt, $type, $options, $fs_freq, $fs_passno) = 
	  split /\s+/;

	print FSTAB_OUT_FILE if $type ne 'swap';
      }

    foreach $entry (@cf_swapinfo)
      {
	if ($entry->{priority} >= 0) 
	  {
	    $priority_str = sprintf "pri=%-11u", $entry->{priority};
	  }
	else
	  {
	    $priority_str = "defaults       ";
	  }

	print FSTAB_OUT_FILE "#" if !$entry->{enabled};
	printf FSTAB_OUT_FILE 
	  "%-23s swap                    swap    %s 0 0\n",
	  $entry->{device},
	  $priority_str;
      }

    close FSTAB_FILE;
  }


sub set
  {
    if ($verbose) 
      { 
	print STDERR "Setting system configuration from XML input.\n"; 
      }

    if ($verbose) { print STDERR "Parsing XML.\n"; }
    xml_parse;
    
    if ($do_immediate)
      {
	if ($verbose) 
	  { 
	    print STDERR 
	      "Changing running configuration via local utilities.\n"; 
	  }
	setup_swap_files;
	set_swap_entries;
      }
  }


# --- Filter config: XML in, XML out --- #


sub filter
  {
    xml_parse;
    xml_print;
  }


# --- Main --- #

$operation = "";		# Major operation user wants to perform. [get | set | filter]


sub set_operation
  {
    if ($operation ne "")
      {
	print STDERR "Error: You may specify only one major operation.\n\n";
	print STDERR $Usage;
	exit(1);
      }
    
    $operation = @_[0];
  }


# Process options.

while (@ARGV)
  {
    if    (@ARGV[0] eq "--get"    || @ARGV[0] eq "-g") { set_operation("get"); }
    elsif (@ARGV[0] eq "--set"    || @ARGV[0] eq "-s") { set_operation("set"); }
    elsif (@ARGV[0] eq "--filter" || @ARGV[0] eq "-f") { set_operation("filter"); }
    elsif (@ARGV[0] eq "--help"   || @ARGV[0] eq "-h") { print $Usage; exit(0); }
    elsif (@ARGV[0] eq "--version")                    { print "$version\n"; exit(0); }
    elsif (@ARGV[0] eq "--prefix" || @ARGV[0] eq "-p")
      {
	if ($prefix ne "")
	  {
	    print STDERR "Error: You may specify --prefix only once.\n\n";
	    print STDERR $Usage; exit(1);
	  }
	
	$prefix = @ARGV[1];
	
	if ($prefix eq "")
	  {
	    print STDERR "Error: You must specify an argument to the --prefix option.\n\n";
	    print STDERR $Usage; exit(1);
	  }
	
	shift @ARGV;		# For the argument.
      }
    elsif (@ARGV[0] eq "--disable-immediate")           { $do_immediate = 0; }
    elsif (@ARGV[0] eq "--verbose" || @ARGV[0] eq "-v") { $verbose = 1; }
    else
      {
	print STDERR "Error: Unrecognized option '@ARGV[0]'.\n\n";
	print STDERR $Usage; exit(1);
      }

    shift @ARGV;
  }


# Do our thing.

if    ($operation eq "get")    { get; }
elsif ($operation eq "set")    { set; }
elsif ($operation eq "filter") { filter; }
else
  {
    print STDERR "Error: No operation specified.\n\n";
    print STDERR $Usage; exit(1);
  }

