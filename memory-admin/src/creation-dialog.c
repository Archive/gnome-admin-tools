/* -*- mode: c; style: linux -*- */

/* creation-dialog.c
 * Copyright (C) 2000 Helix Code, Inc.
 *
 * Written by Bradford Hovinen (hovinen@helixcode.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "creation-dialog.h"
#include "callbacks.h"

enum _swap_type_t {
	ST_PARTITION, ST_FILE
};

typedef enum _swap_type_t swap_type_t;

static swap_type_t get_swap_type (CreationDialog *dialog);
static swap_device_t *create_swap_device (CreationDialog *dialog);

static void use_non_default_priority_toggled_cb (GtkToggleButton *button, 
						 CreationDialog *dialog);
static void create_ok_cb                        (GtkButton *button, 
						 CreationDialog *dialog);
static void create_cancel_cb                    (GtkButton *button, 
						 CreationDialog *dialog);
static void browse_cb                           (GtkButton *button, 
						 CreationDialog *dialog);
static void browse_done_cb                      (BrowseDialog *browser,
						 gchar *path, gint size,
						 CreationDialog *dialog);

CreationDialog *
creation_dialog_new (void (*callback) (CreationDialog *, swap_device_t *, 
				       gpointer),
		     gpointer data)
{
	CreationDialog *dialog;
	GtkWidget *browse_button;

	dialog = g_new0 (CreationDialog, 1);
	dialog->creation_data = 
		read_glade_interface ("swapdev-creation.glade");

	dialog->dialog = 
		GNOME_DIALOG (glade_xml_get_widget 
			      (dialog->creation_data, "create_dialog"));

	gnome_dialog_button_connect (dialog->dialog, 0,
				     GTK_SIGNAL_FUNC (create_ok_cb), 
				     dialog);
	gnome_dialog_button_connect (dialog->dialog, 1,
				     GTK_SIGNAL_FUNC (create_cancel_cb), 
				     dialog);

	dialog->priority_toggle = 
		GTK_TOGGLE_BUTTON 
		(glade_xml_get_widget (dialog->creation_data,
				       "use_non_default_priority_toggle"));
	gtk_signal_connect (GTK_OBJECT (dialog->priority_toggle), "toggled", 
			    GTK_SIGNAL_FUNC 
			    (use_non_default_priority_toggled_cb), dialog);

	browse_button = glade_xml_get_widget (dialog->creation_data,
					      "browse_button");
	gtk_signal_connect (GTK_OBJECT (browse_button), "clicked", 
			    GTK_SIGNAL_FUNC (browse_cb), dialog);

	dialog->type_menu = GTK_OPTION_MENU (glade_xml_get_widget
					     (dialog->creation_data,
					      "type_widget"));
	dialog->path_entry = GTK_ENTRY (glade_xml_get_widget
					(dialog->creation_data,
					 "path_widget"));
	dialog->size_entry = GTK_SPIN_BUTTON (glade_xml_get_widget
					      (dialog->creation_data,
					       "size_widget"));
	dialog->priority_entry = GTK_SPIN_BUTTON (glade_xml_get_widget
						  (dialog->creation_data,
						   "priority_widget"));
	dialog->enable_toggle = GTK_TOGGLE_BUTTON (glade_xml_get_widget
						   (dialog->creation_data,
						    "enable_toggle"));
	dialog->callback = callback;
	dialog->data = data;

	return dialog;
}

void
creation_dialog_destroy (CreationDialog *dialog) 
{
	if (dialog->browse_dialog)
		browse_dialog_destroy (dialog->browse_dialog);
	gnome_dialog_close (dialog->dialog);
	gtk_object_destroy (GTK_OBJECT (dialog->creation_data));
	g_free (dialog);
}

static swap_type_t
get_swap_type (CreationDialog *dialog) 
{
	GtkMenu *menu;
	GtkWidget *active_item;

	menu = GTK_MENU (gtk_option_menu_get_menu (dialog->type_menu));
	active_item = gtk_menu_get_active (menu);

	if (active_item == GTK_MENU_SHELL (menu)->children->data)
		return ST_PARTITION; /* First item -- partition */
	else
		return ST_FILE;      /* Second item -- file */
}

static swap_device_t *
create_swap_device (CreationDialog *dialog) 
{
	swap_device_t *swap_device;

	swap_device = swap_device_new ();
	swap_device->is_new = TRUE;
	swap_device->is_file = (get_swap_type (dialog) == ST_FILE);

	swap_device->device = 
		g_strdup (gtk_entry_get_text (dialog->path_entry));

	swap_device->size = 
		gtk_spin_button_get_value_as_int (dialog->size_entry);

	if (gtk_toggle_button_get_active (dialog->priority_toggle))
		swap_device->priority =
			gtk_spin_button_get_value_as_int 
			(dialog->priority_entry);
	else
		swap_device->priority = -1;

	swap_device->enabled =
		gtk_toggle_button_get_active (dialog->enable_toggle);	

	return swap_device;
}

static void
use_non_default_priority_toggled_cb (GtkToggleButton *button, 
				     CreationDialog *dialog) 
{
	gboolean enable;

	enable = gtk_toggle_button_get_active (button);
	gtk_widget_set_sensitive (GTK_WIDGET (dialog->priority_entry), enable);
}

static void
create_ok_cb (GtkButton *button, CreationDialog *dialog) 
{
	dialog->callback (dialog, create_swap_device (dialog), dialog->data);
	creation_dialog_destroy (dialog);
}

static void
create_cancel_cb (GtkButton *button, CreationDialog *dialog) 
{
	creation_dialog_destroy (dialog);
}

static void
browse_cb (GtkButton *button, CreationDialog *dialog) 
{
	swap_type_t swap_type;
	gchar *path;

	if (dialog->browse_dialog) return;

	swap_type = get_swap_type (dialog);

	path = gtk_entry_get_text (dialog->path_entry);

	if (swap_type == ST_PARTITION)
		dialog->browse_dialog =
			browse_dialog_new_partition 
			(path, BROWSE_CALLBACK (browse_done_cb), dialog);
	else
		dialog->browse_dialog =
			browse_dialog_new_file 
			(path, BROWSE_CALLBACK (browse_done_cb), dialog);
}

static void
browse_done_cb (BrowseDialog *browser, gchar *path, gint size, 
		CreationDialog *dialog) 
{
	if (path)
		gtk_entry_set_text (dialog->path_entry, browser->path);

	if (size)
		gtk_spin_button_set_value (dialog->size_entry, browser->size);

	dialog->browse_dialog = NULL;
}
