/* -*- mode: c; style: linux -*- */

/* browse-dialog.h
 * Copyright (C) 2000 Helix Code, Inc.
 *
 * Written by Bradford Hovinen (hovinen@helixcode.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef __BROWSE_DIALOG_H
#define __BROWSE_DIALOG_H

#include <gtk/gtk.h>

#define BROWSE_DIALOG(obj) ((BrowseDialog *) obj)
#define BROWSE_CALLBACK(func) ((void (*) (BrowseDialog *, gchar *, gint, gpointer)) func)

typedef struct _BrowseDialog BrowseDialog;

struct _BrowseDialog 
{
	GtkWidget *widget;
	void (*callback) (BrowseDialog *, gchar *, gint, gpointer);
	gpointer data;

	char *path;
	gint size;
};

BrowseDialog *browse_dialog_new_partition (gchar *current_path,
					   void (*) (BrowseDialog *,
						     gchar *, gint, 
						     gpointer),
					   gpointer data);
BrowseDialog *browse_dialog_new_file      (gchar *current_path,
					   void (*) (BrowseDialog *, 
						     gchar *, gint, 
						     gpointer),
					   gpointer data);

void          browse_dialog_destroy       (BrowseDialog *widget);

#endif /* __BROWSE_DIALOG_H */
