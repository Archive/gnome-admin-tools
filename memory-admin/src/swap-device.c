/* -*- mode: c; style: linux -*- */

/* swap-device.c
 * Copyright (C) 2000 Helix Code, Inc.
 *
 * Written by Bradford Hovinen (hovinen@helixcode.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include <stdlib.h>

#include "swap-device.h"
#include "xml.h"

swap_device_t *
swap_device_new (void) 
{
	swap_device_t *swap_device;

	swap_device = g_new0 (swap_device_t, 1);
	return swap_device;
}

void
swap_device_destroy (swap_device_t *swap_device) 
{
	g_free (swap_device->device);
	g_free (swap_device);
}

static gboolean
read_boolean (char *str) 
{
	if (!g_strcasecmp (str, "yes") || !g_strcasecmp (str, "true"))
		return TRUE;
	else
		return FALSE;
}

void
swap_device_read (swap_device_t *swap_device, xmlNodePtr swapdev_node) 
{
	xmlNodePtr node;
	xmlNodePtr device_node = NULL;
	xmlNodePtr priority_node = NULL;
	xmlNodePtr enabled_node = NULL;

	node = swapdev_node->childs;

	while (node) {
		if (!strcmp (node->name, "device") && !device_node)
			device_node = node;
		else if (!strcmp (node->name, "priority") && !priority_node)
			priority_node = node;
		else if (!strcmp (node->name, "enabled") && !enabled_node)
			enabled_node = node;
		node = node->next;
	}

	if (!(device_node && priority_node && enabled_node))
		g_warning ("Badly formed XML node");

	swap_device->device = g_strdup (xml_element_get_content (device_node));
	swap_device->priority = atoi (xml_element_get_content (priority_node));
	swap_device->enabled = 
		read_boolean (xmlGetProp (enabled_node, "state"));
}

xmlNodePtr
swap_device_write (swap_device_t *swap_device) 
{
	xmlNodePtr node, child;
	gchar *priority_str, *enabled_str, *size_str;

	if (swap_device->enabled)
		enabled_str = "yes";
	else
		enabled_str = "no";

	priority_str = g_strdup_printf ("%d", swap_device->priority);

	node = xmlNewNode (NULL, "swapdev");
	xmlNewChild (node, NULL, "device", swap_device->device);
	child = xmlNewChild (node, NULL, "enabled", NULL);
	xmlNewProp (child, "state", enabled_str);
	xmlNewChild (node, NULL, "priority", priority_str);

	if (swap_device->size) {
		size_str = g_strdup_printf ("%d", swap_device->size * 1024);
		xmlNewChild (node, NULL, "size", size_str);
		g_free (size_str);
	}

	if (swap_device->is_file) {
		child = xmlNewChild (node, NULL, "isfile", NULL);
		xmlNewProp (child, "state", "yes");
	}

	if (swap_device->is_new) {
		child = xmlNewChild (node, NULL, "isnew", NULL);
		xmlNewProp (child, "state", "yes");
		swap_device->is_new = FALSE;
	}

	g_free (priority_str);

	return node;
}

GList *
read_xml_data (xmlDocPtr doc) 
{
	xmlNodePtr node;
	GList *list_head, *list_tail;
	swap_device_t *swap_device;

	node = xml_doc_get_root (doc)->childs;
	list_head = list_tail = NULL;

	while (node) {
		if (!strcmp (node->name, "swapdev")) {
			swap_device = swap_device_new ();
			swap_device_read (swap_device, node);
			if (swap_device) {
				list_tail = 
					g_list_append (list_tail, swap_device);
				if (list_head)
					list_tail = list_tail->next;
				else
					list_head = list_tail;
			}
		}

		node = node->next;
	}

	return list_head;
}

xmlDocPtr
write_xml_data (GList *swap_devices) 
{
	xmlDocPtr doc;
	xmlNodePtr root;

	doc = xmlNewDoc ("1.0");
	root = xmlNewDocNode (doc, NULL, "memory", NULL);

	while (swap_devices) {
		xmlAddChild (root, swap_device_write (swap_devices->data));
		swap_devices = swap_devices->next;
	}

	xmlDocSetRootElement (doc, root);

	return doc;
}
