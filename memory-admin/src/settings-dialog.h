/* -*- mode: c; style: linux -*- */

/* settings-dialog.h
 * Copyright (C) 2000 Helix Code, Inc.
 *
 * Written by Bradford Hovinen (hovinen@helixcode.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef __SETTINGS_DIALOG_H
#define __SETTINGS_DIALOG_H

#include <gtk/gtk.h>
#include <glade/glade.h>
#include <gnome.h>

#include "swap-device.h"

#define SETTINGS_DIALOG(obj) ((CreationDialog *) obj)
#define SETTINGS_CALLBACK(func) ((void (*) (SettingsDialog *, gpointer)) func)

typedef struct _SettingsDialog SettingsDialog;

struct _SettingsDialog 
{
	GladeXML         *settings_data;

	swap_device_t    *swap_device;

	GnomeDialog      *dialog;
	GtkLabel         *device_label;
	GtkSpinButton    *priority_entry;
	GtkToggleButton  *priority_toggle;
	GtkToggleButton  *enable_toggle;

	void (*callback) (SettingsDialog *, gpointer);
	gpointer data;
};

SettingsDialog *settings_dialog_new (swap_device_t *swap_device,
				     void (*callback) (SettingsDialog *, 
						       gpointer),
				     gpointer data);

void settings_dialog_destroy (SettingsDialog *dialog);

#endif /* __SETTINGS_DIALOG_H */
