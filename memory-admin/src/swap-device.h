/* -*- mode: c; style: linux -*- */

/* swap-device.h
 * Copyright (C) 2000 Helix Code, Inc.
 *
 * Written by Bradford Hovinen (hovinen@helixcode.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef __SWAP_DEVICE_H
#define __SWAP_DEVICE_H

#include <glib.h>
#include <tree.h>

struct _swap_device_t 
{
	char *device;
	gint priority;
	gboolean enabled;
	gint row;
	guint size;
	gboolean is_file;
	gboolean is_new;
};

typedef struct _swap_device_t swap_device_t;

swap_device_t *swap_device_new     (void);
void           swap_device_destroy (swap_device_t *swap_device);
void           swap_device_read    (swap_device_t *swap_device,
				    xmlNodePtr swapdev_node);
xmlNodePtr     swap_device_write   (swap_device_t *swap_device);

GList         *read_xml_data       (xmlDocPtr doc);
xmlDocPtr      write_xml_data      (GList *swap_devices);

#endif /* __SWAP_DEVICE_H */
