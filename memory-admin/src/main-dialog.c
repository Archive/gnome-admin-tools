/* -*- mode: c; style: linux -*- */

/* main-dialog.c
 * Copyright (C) 2000 Helix Code, Inc.
 *
 * Written by Bradford Hovinen (hovinen@helixcode.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gdk-pixbuf/gdk-pixbuf.h>

#include "main-dialog.h"
#include "creation-dialog.h"
#include "settings-dialog.h"
#include "xml.h"
#include "callbacks.h"

#include "checked.xpm"
#include "unchecked.xpm"

static void ok_cb                       (GtkButton       *button,
                                         MainDialog      *dialog);
static void apply_cb                    (GtkButton       *button,
                                         MainDialog      *dialog);
static void cancel_cb                   (GtkButton       *button,
                                         MainDialog      *dialog);
static void help_cb                     (GtkButton       *button,
                                         MainDialog      *dialog);
static void enable_cb                   (GtkButton       *button,
                                         MainDialog      *dialog);
static void disable_cb                  (GtkButton       *button,
                                         MainDialog      *dialog);
static void create_cb                   (GtkButton       *button,
                                         MainDialog      *dialog);
static void remove_cb                   (GtkButton       *button,
                                         MainDialog      *dialog);
static void settings_cb                 (GtkButton       *button,
                                         MainDialog      *dialog);
static void select_row_cb               (GtkCList        *clist, 
					 gint             row, 
					 gint             column,
					 GdkEventButton  *event, 
					 MainDialog      *dialog);

static void create_done_cb              (CreationDialog  *creation_dialog, 
					 swap_device_t   *swap_device,
					 MainDialog      *dialog);
static void settings_done_cb            (SettingsDialog  *settings_dialog, 
					 MainDialog      *dialog);

static void set_pixmap                  (GtkCList        *clist, 
				         swap_device_t   *swap_device, 
				         gint             row);
static void add_swap_device             (MainDialog      *dialog, 
					 swap_device_t   *swap_device);
static void update_clist                (GtkCList        *clist, 
					 swap_device_t   *swap_device);
static void remove_swap_device          (MainDialog      *dialog,
					 swap_device_t   *swap_device);
static void populate_dialog             (MainDialog      *dialog,
					 GList           *swap_devices);
static void set_buttons_sensitive       (MainDialog      *dialog, 
					 gboolean         s);

MainDialog *
main_dialog_new (GList *swap_devices,
		 void (*apply_callback) (MainDialog *, gboolean)) 
{
	MainDialog *dialog;
	GtkWidget *create_button;

	dialog = g_new0 (MainDialog, 1);

	dialog->dialog_data = read_glade_interface ("memory-admin.glade");

	dialog->dialog =
		GNOME_DIALOG (glade_xml_get_widget (dialog->dialog_data, 
						    "memory_admin"));
	dialog->swap_devices_list =
		GTK_CLIST (glade_xml_get_widget (dialog->dialog_data,
						 "swap_areas_list"));
 	dialog->enable_button =
		GTK_BUTTON (glade_xml_get_widget (dialog->dialog_data,
						  "enable_button"));
 	dialog->disable_button =
		GTK_BUTTON (glade_xml_get_widget (dialog->dialog_data,
						  "disable_button"));
 	dialog->remove_button =
		GTK_BUTTON (glade_xml_get_widget (dialog->dialog_data,
						  "remove_button"));
 	dialog->settings_button =
		GTK_BUTTON (glade_xml_get_widget (dialog->dialog_data,
						  "settings_button"));

	dialog->apply_cb = apply_callback;
	dialog->swap_devices = swap_devices;

	create_button = glade_xml_get_widget (dialog->dialog_data,
					      "create_button");

	gnome_dialog_button_connect (dialog->dialog, 1,
				     GTK_SIGNAL_FUNC (ok_cb), dialog);
	gnome_dialog_button_connect (dialog->dialog, 2,
				     GTK_SIGNAL_FUNC (apply_cb), dialog);
	gnome_dialog_button_connect (dialog->dialog, 3,
				     GTK_SIGNAL_FUNC (cancel_cb), dialog);
	gnome_dialog_button_connect (dialog->dialog, 0,
				     GTK_SIGNAL_FUNC (help_cb), dialog);

	gtk_signal_connect (GTK_OBJECT (dialog->swap_devices_list),
			    "select-row", select_row_cb, dialog);
	gtk_signal_connect (GTK_OBJECT (dialog->enable_button),
			    "clicked", enable_cb, dialog);
	gtk_signal_connect (GTK_OBJECT (dialog->disable_button),
			    "clicked", disable_cb, dialog);
	gtk_signal_connect (GTK_OBJECT (create_button),
			    "clicked", create_cb, dialog);
	gtk_signal_connect (GTK_OBJECT (dialog->remove_button),
			    "clicked", remove_cb, dialog);
	gtk_signal_connect (GTK_OBJECT (dialog->settings_button),
			    "clicked", settings_cb, dialog);

	populate_dialog (dialog, swap_devices);

	return dialog;
}

void 
main_dialog_destroy (MainDialog *dialog) 
{
	gnome_dialog_close (dialog->dialog);
	gtk_object_destroy (GTK_OBJECT (dialog->dialog_data));
	g_free (dialog);
}

static void
ok_cb (GtkButton *button, MainDialog *dialog)
{
	dialog->apply_cb (dialog, FALSE);
	gtk_main_quit ();   /* FIXME */
}


static void
apply_cb (GtkButton *button, MainDialog *dialog)
{
	dialog->apply_cb (dialog, FALSE);
}


static void
cancel_cb (GtkButton *button, MainDialog *dialog)
{
	dialog->apply_cb (dialog, TRUE);
	gtk_main_quit ();   /* FIXME */
}


static void 
help_cb (GtkButton *button, MainDialog *dialog) 
{
	gchar *tmp;

	tmp = gnome_help_file_find_file ("users-guide", "memory-admin.html");

	if (tmp) {
		gnome_help_goto (0, tmp);
		g_free (tmp);
	} else {
		GtkWidget *mbox;

		mbox = gnome_message_box_new
			(_("No help is available/installed. Please " \
			   "make sure you\nhave the GNOME User's " \
			   "Guide installed on your system."),
			 GNOME_MESSAGE_BOX_ERROR, _("Close"), NULL);

		gtk_widget_show (mbox);
	}
}

static void
enable_cb (GtkButton *button, MainDialog *dialog)
{
	dialog->current_swap_device->enabled = TRUE;
	set_pixmap (dialog->swap_devices_list, dialog->current_swap_device,
		    gtk_clist_find_row_from_data 
		    (dialog->swap_devices_list, dialog->current_swap_device));
}


static void
disable_cb (GtkButton *button, MainDialog *dialog)
{
	dialog->current_swap_device->enabled = FALSE;
	set_pixmap (dialog->swap_devices_list, dialog->current_swap_device,
		    gtk_clist_find_row_from_data 
		    (dialog->swap_devices_list, dialog->current_swap_device));
}

static void
create_cb (GtkButton *button, MainDialog *dialog) 
{
	creation_dialog_new (CREATION_CALLBACK (create_done_cb), dialog);
}

static void
remove_cb (GtkButton *button, MainDialog *dialog)
{
	remove_swap_device (dialog, dialog->current_swap_device);
}

static void
settings_cb (GtkButton *button, MainDialog *dialog) 
{
	settings_dialog_new (dialog->current_swap_device, 
			     SETTINGS_CALLBACK (settings_done_cb), dialog);
}

static void
select_row_cb (GtkCList *clist, gint row, gint column, 
	       GdkEventButton *event, MainDialog *dialog) 
{
	dialog->current_swap_device = gtk_clist_get_row_data (clist, row);
}

static void
create_done_cb (CreationDialog *creation_dialog, swap_device_t *swap_device,
		MainDialog *dialog) 
{
	dialog->swap_devices = 
		g_list_append (dialog->swap_devices, swap_device);
	add_swap_device (dialog, swap_device);
}

static void
settings_done_cb (SettingsDialog *settings_dialog, MainDialog *dialog) 
{
	update_clist (dialog->swap_devices_list, settings_dialog->swap_device);
}

static void
set_pixmap (GtkCList *clist, swap_device_t *swap_device, gint row) 
{
	GdkPixbuf *pixbuf;
	GdkPixmap *pixmap;
	GdkBitmap *mask;
	static GdkPixmap *checked_pixmap = NULL, *unchecked_pixmap = NULL;
	static GdkBitmap *checked_mask = NULL, *unchecked_mask = NULL;

	if (swap_device->enabled) {
		if (!checked_pixmap) {
			pixbuf = gdk_pixbuf_new_from_xpm_data (checked_xpm);
			gdk_pixbuf_render_pixmap_and_mask (pixbuf,
							   &checked_pixmap, 
							   &checked_mask, 1);
		}
 		pixmap = checked_pixmap;
		mask = checked_mask;
	} else {
		if (!unchecked_pixmap) {
			pixbuf = gdk_pixbuf_new_from_xpm_data (unchecked_xpm);
			gdk_pixbuf_render_pixmap_and_mask (pixbuf,
							   &unchecked_pixmap, 
							   &unchecked_mask, 1);
		}
 		pixmap = unchecked_pixmap;
		mask = unchecked_mask;
	}

	gtk_clist_set_pixmap (clist, row, 0, pixmap, mask);
}

static void
add_swap_device (MainDialog *dialog, swap_device_t *swap_device)
{
	char *cols[3];
	gint row;

	cols[0] = NULL;
	cols[1] = swap_device->device;
	if (swap_device->priority == -1)
		cols[2] = "default";
	else
		cols[2] = g_strdup_printf ("%d", swap_device->priority);

	row = gtk_clist_append (dialog->swap_devices_list, cols);
	set_pixmap (dialog->swap_devices_list, swap_device, row);
	swap_device->row = row;
	gtk_clist_set_row_data (dialog->swap_devices_list, row, swap_device);

	if (swap_device->priority != -1)
		g_free (cols[2]);

	set_buttons_sensitive (dialog, TRUE);
}

static void
update_clist (GtkCList *clist, swap_device_t *swap_device) 
{
	gint row;
	gchar *priority_str;

	if (swap_device->priority != -1)
		priority_str = g_strdup_printf
			("%d", swap_device->priority);
	else
		priority_str = "default";

	row = gtk_clist_find_row_from_data (clist, swap_device);
	gtk_clist_set_text (clist, row, 1, swap_device->device);
	gtk_clist_set_text (clist, row, 2, priority_str);

	if (swap_device->priority != -1)
		g_free (priority_str);

	set_pixmap (clist, swap_device,
		    gtk_clist_find_row_from_data (clist, swap_device));
}

static void
remove_swap_device (MainDialog *dialog, swap_device_t *swap_device) 
{
	gint row;

	row = gtk_clist_find_row_from_data (dialog->swap_devices_list,
					    swap_device);
	dialog->swap_devices = g_list_remove (dialog->swap_devices, 
					      swap_device);
	gtk_clist_remove (dialog->swap_devices_list, row);

	if (swap_device == dialog->current_swap_device) {
		if (row == dialog->swap_devices_list->rows)
			row--;

		if (row >= 0) {
			gtk_clist_select_row
				(dialog->swap_devices_list, row, 0);
			dialog->current_swap_device = gtk_clist_get_row_data
				(dialog->swap_devices_list, row);
		} else {
			set_buttons_sensitive (dialog, FALSE);
		}
	}

	swap_device_destroy (swap_device);
}

static void
populate_dialog (MainDialog *dialog, GList *swap_devices) 
{
	GList *node;

	node = swap_devices;

	while (node) {
		add_swap_device (dialog, node->data);
		node = node->next;
	}

	if (swap_devices)
		gtk_clist_select_row (dialog->swap_devices_list, 0, 0);
	else
		set_buttons_sensitive (dialog, FALSE);
}

static void
set_buttons_sensitive (MainDialog *dialog, gboolean s) 
{
	gtk_widget_set_sensitive (GTK_WIDGET (dialog->enable_button), s);
	gtk_widget_set_sensitive (GTK_WIDGET (dialog->disable_button), s);
	gtk_widget_set_sensitive (GTK_WIDGET (dialog->remove_button), s);
	gtk_widget_set_sensitive (GTK_WIDGET (dialog->settings_button), s);
}
