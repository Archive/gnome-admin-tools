/* -*- mode: c; style: linux -*- */

/* browse-dialog.c
 * Copyright (C) 2000 Helix Code, Inc.
 *
 * Written by Bradford Hovinen (hovinen@helixcode.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "browse-dialog.h"
#include "callbacks.h"

struct _PartitionBrowseDialog
{
	BrowseDialog widget;
	GtkCList *clist;
};

typedef struct _PartitionBrowseDialog PartitionBrowseDialog;

struct _FileBrowseDialog 
{
	BrowseDialog widget;
};

typedef struct _FileBrowseDialog FileBrowseDialog;

static void populate_partition_list (GtkCList *clist, gchar *current_path);

static void partition_select_cb (GtkCList *clist, gint row, gint column,
				 GdkEventButton *event,
				 PartitionBrowseDialog *widget);
static void partition_ok_cb (GtkWidget *dialog, 
			     PartitionBrowseDialog *widget);
static void partition_cancel_cb (GtkWidget *dialog, 
				 PartitionBrowseDialog *widget);
static void file_ok_cb (GtkWidget *dialog, FileBrowseDialog *widget);
static void file_cancel_cb (GtkWidget *dialog, FileBrowseDialog *widget);

BrowseDialog *
browse_dialog_new_partition (gchar *current_path,
			     void (*callback) (BrowseDialog *, 
					       gchar *, gint,
					       gpointer),
			     gpointer data) 
{
	PartitionBrowseDialog *widget;
	GladeXML *glade_data;

	widget = g_new0 (PartitionBrowseDialog, 1);
	glade_data = read_glade_interface ("browse-partition.glade");
	BROWSE_DIALOG (widget)->widget = 
		glade_xml_get_widget (glade_data, "browse_dialog");
	BROWSE_DIALOG (widget)->callback = callback;
	BROWSE_DIALOG (widget)->data = data;
	widget->clist = GTK_CLIST
		(glade_xml_get_widget (glade_data, "partition_list"));
	populate_partition_list (widget->clist, current_path);
	gtk_signal_connect (GTK_OBJECT (widget->clist), "select-row",
			    GTK_SIGNAL_FUNC (partition_select_cb),
			    widget);

	gnome_dialog_button_connect 
		(GNOME_DIALOG (BROWSE_DIALOG (widget)->widget),
		 0, GTK_SIGNAL_FUNC (partition_ok_cb), 
		 widget);

	gnome_dialog_button_connect 
		(GNOME_DIALOG (BROWSE_DIALOG (widget)->widget),
		 1, GTK_SIGNAL_FUNC (partition_cancel_cb), 
		 widget);

	return BROWSE_DIALOG (widget);
}

BrowseDialog *
browse_dialog_new_file (gchar *current_path,
			void (*callback) (BrowseDialog *, 
					  gchar *, gint,
					  gpointer),
			gpointer data) 
{
	FileBrowseDialog *widget;

	widget = g_new0 (FileBrowseDialog, 1);
	BROWSE_DIALOG (widget)->widget =
		gtk_file_selection_new 
		(_("Please select the file that you would like to use"));
	BROWSE_DIALOG (widget)->callback = callback;
	BROWSE_DIALOG (widget)->data = data;

	if (current_path)
		gtk_file_selection_set_filename 
			(GTK_FILE_SELECTION (BROWSE_DIALOG(widget)->widget),
			 current_path);

	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION
					(BROWSE_DIALOG (widget)->
					 widget)->ok_button),
			    "clicked", GTK_SIGNAL_FUNC (file_ok_cb), 
			    widget);

	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION
					(BROWSE_DIALOG (widget)->
					 widget)->cancel_button),
			    "clicked", GTK_SIGNAL_FUNC (file_cancel_cb), 
			    widget);

	gtk_widget_show (BROWSE_DIALOG (widget)->widget);

	return BROWSE_DIALOG (widget);
}

void
browse_dialog_destroy (BrowseDialog *widget) 
{
	gtk_object_destroy (GTK_OBJECT (widget->widget));
	if (widget->path) g_free (widget->path);
	g_free (widget);
}

static void 
populate_partition_list (GtkCList *clist, gchar *current_path) 
{
	/* AAAAAAAAAAAAAAARRRRRRRRRRRRRRRRRGGGGGGGGGGGGGGGGGGGHHHHHHH
	 * HHHHHH!!!!!!!!!!!!!!!!!!!! */
	FILE *in;
	int dummy1, dummy2, size, row;
	char name[32];
	char *array[] = { name };

	in = fopen ("/proc/partitions", "r");

	while (fgetc (in) != '\n');    /* Skip first two lines */
	while (fgetc (in) != '\n');

	while (fscanf (in, "%d %d %d %s\n", 
		       &dummy1, &dummy2, &size, name) != EOF) 
	{
		if (size < 10 || size > 2097152) continue;
		row = gtk_clist_append (clist, array);
		if (current_path && !strcmp (name, current_path +
					     strlen (current_path) -
					     strlen (name)))
			gtk_clist_select_row (clist, row, 0);
		gtk_clist_set_row_data (clist, row, (gpointer) (size / 1024));
	}

	fclose (in);
}

static void 
partition_select_cb (GtkCList *clist, gint row, gint column, 
		     GdkEventButton *event, PartitionBrowseDialog *widget) 
{
	char *text;

	if (gtk_clist_get_text (clist, row, column, &text))
		BROWSE_DIALOG (widget)->path = 
			g_strdup_printf ("/dev/%s", text);
	BROWSE_DIALOG (widget)->size =
		(gint) gtk_clist_get_row_data (clist, row);
}

static void 
partition_ok_cb (GtkWidget *button, PartitionBrowseDialog *widget) 
{
	BROWSE_DIALOG (widget)->callback (BROWSE_DIALOG (widget),
					  BROWSE_DIALOG (widget)->path,
					  BROWSE_DIALOG (widget)->size,
					  BROWSE_DIALOG (widget)->data);
	browse_dialog_destroy (BROWSE_DIALOG (widget));
}

static void 
partition_cancel_cb (GtkWidget *button, PartitionBrowseDialog *widget) 
{
	BROWSE_DIALOG (widget)->callback (BROWSE_DIALOG (widget),
					  NULL, 0,
					  BROWSE_DIALOG (widget)->data);
	browse_dialog_destroy (BROWSE_DIALOG (widget));
}

static void 
file_ok_cb (GtkWidget *button, FileBrowseDialog *widget) 
{
	BROWSE_DIALOG (widget)->path =
		g_strdup (gtk_file_selection_get_filename 
			  (GTK_FILE_SELECTION (BROWSE_DIALOG 
					       (widget)->widget)));

	BROWSE_DIALOG (widget)->callback (BROWSE_DIALOG (widget),
					  BROWSE_DIALOG (widget)->path,
					  BROWSE_DIALOG (widget)->size,
					  BROWSE_DIALOG (widget)->data);
	browse_dialog_destroy (BROWSE_DIALOG (widget));
}

static void
file_cancel_cb (GtkWidget *button, FileBrowseDialog *widget) 
{
	BROWSE_DIALOG (widget)->callback (BROWSE_DIALOG (widget),
					  NULL, 0,
					  BROWSE_DIALOG (widget)->data);
	browse_dialog_destroy (BROWSE_DIALOG (widget));
}
