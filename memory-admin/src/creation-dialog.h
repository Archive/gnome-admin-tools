/* -*- mode: c; style: linux -*- */

/* creation-dialog.h
 * Copyright (C) 2000 Helix Code, Inc.
 *
 * Written by Bradford Hovinen (hovinen@helixcode.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef __CREATION_DIALOG_H
#define __CREATION_DIALOG_H

#include <gtk/gtk.h>
#include <glade/glade.h>
#include <gnome.h>

#include "swap-device.h"
#include "browse-dialog.h"

#define CREATION_DIALOG(obj) ((CreationDialog *) obj)
#define CREATION_CALLBACK(func) ((void (*) (CreationDialog *, swap_device_t *, gpointer)) func)

typedef struct _CreationDialog CreationDialog;

struct _CreationDialog 
{
	GladeXML         *creation_data;

	GnomeDialog      *dialog;
	GtkOptionMenu    *type_menu;
	GtkEntry         *path_entry;
	GtkSpinButton    *size_entry;
	GtkSpinButton    *priority_entry;
	GtkToggleButton  *priority_toggle;
	GtkToggleButton  *enable_toggle;

	BrowseDialog     *browse_dialog;

	void (*callback) (CreationDialog *, swap_device_t *, gpointer);
	gpointer data;
};

CreationDialog *creation_dialog_new (void (*callback) (CreationDialog *, 
						       swap_device_t *, 
						       gpointer),
				     gpointer data);

void creation_dialog_destroy (CreationDialog *dialog);

#endif /* __CREATION_DIALOG_H */
