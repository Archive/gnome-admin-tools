/* -*- mode: c; style: linux -*- */

/* main-dialog.h
 * Copyright (C) 2000 Helix Code, Inc.
 *
 * Written by Bradford Hovinen (hovinen@helixcode.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef __MAIN_DIALOG_H
#define __MAIN_DIALOG_H

#include <gtk/gtk.h>
#include <gnome.h>
#include <glade/glade.h>

#include "swap-device.h"

#define MAIN_DIALOG(obj) ((MainDialog *) obj)

typedef struct _MainDialog MainDialog;

struct _MainDialog 
{
	GladeXML *dialog_data;

	GList *swap_devices;

	GnomeDialog *dialog;
	GtkCList *swap_devices_list;

	GtkButton *enable_button;
	GtkButton *disable_button;
	GtkButton *remove_button;
	GtkButton *settings_button;

	swap_device_t *current_swap_device;

	void (*apply_cb) (MainDialog *, gboolean);
};

MainDialog *main_dialog_new (GList *swap_devices,
			     void (*apply_cb) (MainDialog *, gboolean));

void main_dialog_destroy (MainDialog *dialog);

#endif /* __MAIN_DIALOG_H */
