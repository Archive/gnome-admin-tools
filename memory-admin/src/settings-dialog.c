/* -*- mode: c; style: linux -*- */

/* settings-dialog.c
 * Copyright (C) 2000 Helix Code, Inc.
 *
 * Written by Bradford Hovinen (hovinen@helixcode.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "settings-dialog.h"
#include "callbacks.h"

static void populate_settings_dialog            (SettingsDialog *dialog, 
						 swap_device_t *swap_device);
static void settings_save                       (SettingsDialog *dialog);

static void use_non_default_priority_toggled_cb (GtkToggleButton *button, 
						 SettingsDialog *dialog);
static void settings_ok_cb                      (GtkButton *button, 
						 SettingsDialog *dialog);
static void settings_cancel_cb                  (GtkButton *button, 
						 SettingsDialog *dialog);

SettingsDialog *
settings_dialog_new (swap_device_t *swap_device,
		     void (*callback) (SettingsDialog *, gpointer),
		     gpointer data)
{
	SettingsDialog *dialog;

	dialog = g_new0 (SettingsDialog, 1);

	dialog->settings_data = 
		read_glade_interface ("swapdev-settings.glade");

	dialog->dialog = 
		GNOME_DIALOG (glade_xml_get_widget 
			      (dialog->settings_data, "settings_dialog"));

	gnome_dialog_button_connect (dialog->dialog, 0,
				     GTK_SIGNAL_FUNC (settings_ok_cb), 
				     dialog);
	gnome_dialog_button_connect (dialog->dialog, 1,
				     GTK_SIGNAL_FUNC (settings_cancel_cb), 
				     dialog);

	dialog->priority_toggle = 
		GTK_TOGGLE_BUTTON 
		(glade_xml_get_widget (dialog->settings_data,
				       "use_non_default_priority_toggle"));
	gtk_signal_connect (GTK_OBJECT (dialog->priority_toggle), "toggled", 
			    GTK_SIGNAL_FUNC 
			    (use_non_default_priority_toggled_cb), dialog);

	dialog->device_label = GTK_LABEL (glade_xml_get_widget
					  (dialog->settings_data,
					   "swap_device_label"));
	dialog->priority_entry = GTK_SPIN_BUTTON (glade_xml_get_widget
						  (dialog->settings_data,
						   "priority_widget"));
	dialog->enable_toggle = GTK_TOGGLE_BUTTON (glade_xml_get_widget
						   (dialog->settings_data,
						    "enable_toggle"));
	dialog->swap_device = swap_device;
	dialog->callback = callback;
	dialog->data = data;

	populate_settings_dialog (dialog, swap_device);

	return dialog;
}

void
settings_dialog_destroy (SettingsDialog *dialog) 
{
	gnome_dialog_close (dialog->dialog);
	gtk_object_destroy (GTK_OBJECT (dialog->settings_data));
	g_free (dialog);
}

static void
populate_settings_dialog (SettingsDialog *dialog, swap_device_t *swap_device) 
{
	gtk_label_set_text (dialog->device_label, swap_device->device);
	gtk_toggle_button_set_active (dialog->enable_toggle,
				      swap_device->enabled);
	gtk_toggle_button_set_active (dialog->priority_toggle,
				      swap_device->priority != -1);

	if (swap_device->priority != -1)
		gtk_spin_button_set_value (dialog->priority_entry,
					   swap_device->priority);
	else
		gtk_widget_set_sensitive (GTK_WIDGET (dialog->priority_entry),
					  FALSE);
}

static void
settings_save (SettingsDialog *dialog) 
{
	if (gtk_toggle_button_get_active (dialog->priority_toggle))
		dialog->swap_device->priority =
			gtk_spin_button_get_value_as_int
			(dialog->priority_entry);
	else
		dialog->swap_device->priority = -1;

	dialog->swap_device->enabled =
		gtk_toggle_button_get_active (dialog->enable_toggle);
}

static void
use_non_default_priority_toggled_cb (GtkToggleButton *button, 
				     SettingsDialog *dialog) 
{
	gboolean enable;

	enable = gtk_toggle_button_get_active (button);
	gtk_widget_set_sensitive (GTK_WIDGET (dialog->priority_entry), enable);
}

static void
settings_ok_cb (GtkButton *button, SettingsDialog *dialog) 
{
	settings_save (dialog);
	dialog->callback (dialog, dialog->data);
	settings_dialog_destroy (dialog);
}

static void
settings_cancel_cb (GtkButton *button, SettingsDialog *dialog) 
{
	settings_dialog_destroy (dialog);
}
