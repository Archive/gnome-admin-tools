#include <config.h>

#include <stdio.h>

#include <gtk/gtk.h>
#include "xml.h"
#include "transfer.h"

#include <glade/glade.h>

extern GladeXML *glade_interface;
xmlDocPtr root;

TransStringEntry transfer_string_entry_table[] =
{
  { "string", "entry1", "togglebutton1" },
  { 0, 0, 0, 0 }
};


TransStringIPEntry transfer_string_ip_table[] =
{
  { "ip", "entry2", "entry3", "entry4", "entry5", NULL },
  { 0, 0, 0, 0, 0, 0 }
};


TransStringList transfer_string_list_table[] =
{
  { "list", "list1" },
  { 0, 0 }
};


char *field_strings[] = { "field1", "field2" };

TransStringCList transfer_string_clist_table[] =
{
  { "clist", 2, field_strings, "clist1" },
  { 0, 0, 0, 0 }
};

/* non-test */

char *user_fields[]  = { "uid", "name", "info" };
char *group_fields[] = { "gid", "name", "member" };

TransStringCList clist_table[] = 
{
  { "user", 3, user_fields, "users" },
  { "group", 3, group_fields, "groups" },
  { 0, 0, 0, 0 }
};

/*
TransTree trans_tree = {
  transfer_string_entry_table,
  transfer_string_list_table,
  transfer_string_clist_table,
  transfer_string_ip_table
};
*/

TransTree trans_tree = {
  0,
  0,
  clist_table,
  0
};

int delete_event(GtkWidget *widget, gpointer gdata)
{
  transfer_gui_to_xml(&trans_tree, xml_doc_get_root(root));
  xmlDocDump(stdout, root);
  gtk_main_quit();
  return FALSE;
}

void
read_glade_interface(gchar *interface_name)
{
  gchar *path;
  
  path = g_strjoin("/", SCRIPT_DIR, interface_name, NULL);
  /*  printf("The path: %s\n", path); */
  glade_interface = glade_xml_new(path, NULL);
  if (!glade_interface) g_error("Can't find glade interface file.");
}

int main(int argc, char *argv[])
{
  GtkWidget *window;

  gnome_init("user-admin", "0.1", argc, argv);
  glade_gnome_init();

  read_glade_interface("user-admin.glade");
  root = xml_doc_read_from_backend("user-conf");
  if (!root) g_error("Unable to read from backend.");

  window = glade_xml_get_widget(glade_interface, "user-admin");
  gtk_signal_connect( GTK_OBJECT(window), "delete_event", delete_event, NULL);
  transfer_xml_to_gui(&trans_tree, xml_doc_get_root(root));
  gtk_widget_show(window);
  gtk_main();
  return 0;
}
