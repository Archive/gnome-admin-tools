static void transfer_string_clist2_xml_to_gui(TransTree *trans_tree, xmlNodePtr root)
{
  int i;
  xmlNodePtr node, nodesub;
  char *s, *entry[3];
  GtkWidget *clist;
  TransStringCList2 *transfer_string_clist2_table;

  entry[2] = NULL;
  transfer_string_clist2_table = trans_tree->transfer_string_clist2_table;
  for (i = 0; transfer_string_clist2_table[i].xml_path; i++)
  {
    for (node = xml_element_find_first(root, transfer_string_clist2_table[i].xml_path); node;
         node = xml_element_find_next(node, transfer_string_clist2_table[i].xml_path))
    {
      for (entry[0] = NULL, nodesub = xml_element_find_first(node,
             transfer_string_clist2_table[i].xml_path_field_1); nodesub;
	   nodesub = xml_element_find_next(nodesub,
             transfer_string_clist2_table[i].xml_path_field_1))
      {
        if ((s = xml_element_get_content(nodesub))) 
        {
          if (!entry[0]) entry[0] = s;
          else { entry[0] = g_strjoin(" ", entry[0], s, NULL); }
        }
      }

      if (!entry[0]) continue;

      for (entry[1] = NULL, nodesub = xml_element_find_first(node,
                      transfer_string_clist2_table[i].xml_path_field_2); nodesub;
	   nodesub = xml_element_find_next(nodesub,
           transfer_string_clist2_table[i].xml_path_field_2))
      {
        if ((s = xml_element_get_content(nodesub))) 
        {
          if (!entry[1]) entry[1] = s;
          else { entry[1] = g_strjoin(" ", entry[1], s, NULL); }
        }
      }

      if (!entry[1]) continue;

      clist = glade_xml_get_widget(glade_interface, transfer_string_clist2_table[i].clist);
      gtk_clist_append(GTK_CLIST(clist), entry);
      g_free(entry[0]);
      g_free(entry[1]);
    }
  }
}

static void transfer_string_clist2_gui_to_xml(TransTree *trans_tree, xmlNodePtr root)
{
  int i, j, row;
  int col0_added;
  gchar *col0, *col1;
  gchar **col1_elem;
  xmlNodePtr node, node2;
  GtkWidget *clist;

  TransStringCList2 *transfer_string_clist2_table;

  transfer_string_clist2_table = trans_tree->transfer_string_clist2_table;
  for (i = 0; transfer_string_clist2_table[i].xml_path; i++)
  {
    /* First remove any old branches in the XML tree */

    xml_element_destroy_children_by_name(root, transfer_string_clist2_table[i].xml_path);

    /* Add branches corresponding to listed data */


    clist = glade_xml_get_widget(glade_interface, transfer_string_clist2_table[i].clist);
    for (row = 0; gtk_clist_get_text(GTK_CLIST(clist), row, 0, &col0); row++)
    {
      if (!gtk_clist_get_text(GTK_CLIST(clist), row, 1, &col1))
        continue;

      if (!strlen(col1)) continue;

      /* Enclosing element */
      node = xml_element_add(root, transfer_string_clist2_table[i].xml_path);

      col1_elem = g_strsplit(col1, " ", 0);

      for (j = 0, col0_added = 0; col1_elem[j]; j++)
      {
        if (!strlen(col1_elem[j])) continue;
        if (!col0_added)
        {
          node2 = xml_element_add(node, transfer_string_clist2_table[i].xml_path_field_1);
          xml_element_set_content(node2, col0);
        }
        node2 = xml_element_add(node, transfer_string_clist2_table[i].xml_path_field_2);
        xml_element_set_content(node2, col1_elem[j]);
      }

      g_strfreev(col1_elem);
    }
  }
}

void transfer_user_to_gui(xmlNodePtr node)
{
  
}

void transfer_gui_to_user(xmlNodePtr node)
{

}
