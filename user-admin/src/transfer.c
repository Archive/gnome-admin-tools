/* Functions for transferring information between XML tree and UI */

#include <gnome.h>
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>
#include <glade/glade.h>
#include "xml.h"
#include "transfer.h"

GladeXML *glade_interface;


static void transfer_string_entry_xml_to_gui(TransTree *trans_tree, xmlNodePtr root)
{
  int i;
  xmlNodePtr node;
  char *s;
  GtkWidget *editable;
  GtkWidget *toggle;
  TransStringEntry *transfer_string_entry_table;

  transfer_string_entry_table = trans_tree->transfer_string_entry_table;
  if (!transfer_string_entry_table) return;
  for (i = 0; transfer_string_entry_table[i].xml_path; i++)
  {
    node = xml_element_find_first(root, transfer_string_entry_table[i].xml_path);

    if (node && (s = xml_element_get_content(node)))
    {
      editable = glade_xml_get_widget(glade_interface, transfer_string_entry_table[i].editable);
      gtk_entry_set_text(GTK_ENTRY(editable), s);

      if (transfer_string_entry_table[i].toggle) {
	toggle = glade_xml_get_widget(glade_interface, transfer_string_entry_table[i].toggle);
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(toggle), TRUE);
      }
      free(s);
    }
  }
}


static void transfer_string_entry_gui_to_xml(TransTree *trans_tree, xmlNodePtr root)
{
  int i;
  xmlNodePtr node;
  gchar *content;
  GtkWidget *editable;
  GtkWidget *toggle;
  TransStringEntry *transfer_string_entry_table;

  transfer_string_entry_table = trans_tree->transfer_string_entry_table;
  if (!transfer_string_entry_table) return;
  for (i = 0; transfer_string_entry_table[i].xml_path; i++)
  {
    node = xml_element_find_first(root, transfer_string_entry_table[i].xml_path);
    if (transfer_string_entry_table[i].toggle) {
      toggle = glade_xml_get_widget(glade_interface, transfer_string_entry_table[i].toggle);
      if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(toggle))) continue;
    }
    if (!node) node = xml_element_add(root, transfer_string_entry_table[i].xml_path);

    editable = glade_xml_get_widget(glade_interface, transfer_string_entry_table[i].editable);
    content = gtk_editable_get_chars(GTK_EDITABLE(editable), 0, -1);
    xml_element_set_content(node, content);
    g_free(content);
  }
}


static void transfer_string_ip_xml_to_gui(TransTree *trans_tree, xmlNodePtr root)
{
  int i;
  xmlNodePtr node;
  char *s;
  gchar **ip;
  GtkWidget *editable_1;
  GtkWidget *editable_2;
  GtkWidget *editable_3;
  GtkWidget *editable_4;
  GtkWidget *toggle;
  TransStringIPEntry *transfer_string_ip_table;

  transfer_string_ip_table = trans_tree->transfer_string_ip_table;
  if (!transfer_string_ip_table) return;
  for (i = 0; transfer_string_ip_table[i].xml_path; i++)
  {
    node = xml_element_find_first(root, transfer_string_ip_table[i].xml_path);

    if (node && (s = xml_element_get_content(node)))
    {
      ip = g_strsplit(s, ".", 0);

      if (ip[0] && ip[1] && ip[2] && ip[3])
      {
	editable_1 = glade_xml_get_widget(glade_interface, transfer_string_ip_table[i].editable_1);
	editable_2 = glade_xml_get_widget(glade_interface, transfer_string_ip_table[i].editable_2);
	editable_3 = glade_xml_get_widget(glade_interface, transfer_string_ip_table[i].editable_3);
	editable_4 = glade_xml_get_widget(glade_interface, transfer_string_ip_table[i].editable_4);

        gtk_entry_set_text(GTK_ENTRY(editable_1), ip[0]);
        gtk_entry_set_text(GTK_ENTRY(editable_2), ip[1]);
        gtk_entry_set_text(GTK_ENTRY(editable_3), ip[2]);
        gtk_entry_set_text(GTK_ENTRY(editable_4), ip[3]);

        if (transfer_string_ip_table[i].toggle) 
	{
	  toggle = glade_xml_get_widget(glade_interface, transfer_string_ip_table[i].toggle);
          gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(toggle), TRUE);
	}
      }

      g_strfreev(ip);
      free(s);
    }
  }
}


static void transfer_string_ip_gui_to_xml(TransTree *trans_tree, xmlNodePtr root)
{
  int i;
  xmlNodePtr node;
  gchar *content;
  gchar **ip;
  GtkWidget *editable_1;
  GtkWidget *editable_2;
  GtkWidget *editable_3;
  GtkWidget *editable_4;
  GtkWidget *toggle;

  TransStringIPEntry *transfer_string_ip_table;
  transfer_string_ip_table = trans_tree->transfer_string_ip_table;
  if (!transfer_string_ip_table) return;
  for (i = 0; transfer_string_ip_table[i].xml_path; i++)
  {
    node = xml_element_find_first(root, transfer_string_ip_table[i].xml_path);
    if (node) xml_element_destroy(node);

    if (transfer_string_ip_table[i].toggle) {
      toggle = glade_xml_get_widget(glade_interface, transfer_string_ip_table[i].toggle);
      if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(toggle))) continue;
    }
  

    ip = calloc(5, sizeof(gchar *));
    ip[4] = 0;

    editable_1 = glade_xml_get_widget(glade_interface, transfer_string_ip_table[i].editable_1);
    editable_2 = glade_xml_get_widget(glade_interface, transfer_string_ip_table[i].editable_2);
    editable_3 = glade_xml_get_widget(glade_interface, transfer_string_ip_table[i].editable_3);
    editable_4 = glade_xml_get_widget(glade_interface, transfer_string_ip_table[i].editable_4);

    ip[0] = gtk_editable_get_chars(GTK_EDITABLE(editable_1), 0, -1);
    ip[1] = gtk_editable_get_chars(GTK_EDITABLE(editable_2), 0, -1);
    ip[2] = gtk_editable_get_chars(GTK_EDITABLE(editable_3), 0, -1);
    ip[3] = gtk_editable_get_chars(GTK_EDITABLE(editable_4), 0, -1);

    if (strlen(ip[0]) && strlen(ip[1]) && strlen(ip[2]) && strlen(ip[3]))
    {
      content = g_strjoin(".", ip[0], ip[1], ip[2], ip[3], NULL);
      node = xml_element_add(root, transfer_string_ip_table[i].xml_path);
      xml_element_set_content(node, content);
      g_free(content);
    }

    g_strfreev(ip);
  }
}


static void transfer_string_list_xml_to_gui(TransTree *trans_tree, xmlNodePtr root)
{
  int i;
  xmlNodePtr node;
  char *s;
  GList *list_add;
  GtkWidget *item;
  GtkWidget *list;

  TransStringList *transfer_string_list_table;

  transfer_string_list_table = trans_tree->transfer_string_list_table;
  if (!transfer_string_list_table) return;
  for (i = 0; transfer_string_list_table[i].xml_path; i++)
  {
    list_add = 0;

    for (node = xml_element_find_first(root, transfer_string_list_table[i].xml_path);
         node;
         node = xml_element_find_next(node, transfer_string_list_table[i].xml_path))
    {
      if ((s = xml_element_get_content(node))) 
      {
        item = gtk_list_item_new_with_label(s);
        gtk_widget_show(item);
        list_add = g_list_append(list_add, item);
        free(s);
      }
    }

    if (list_add) {
      list = glade_xml_get_widget(glade_interface, transfer_string_list_table[i].list);
      gtk_list_append_items(GTK_LIST(list), list_add);
    }
  }
}


static void transfer_string_list_gui_to_xml(TransTree *trans_tree, xmlNodePtr root)
{
  int i;
  GList *item;
  gchar *text;
  xmlNodePtr node;
  GtkWidget *list;

  TransStringList *transfer_string_list_table;

  transfer_string_list_table = trans_tree->transfer_string_list_table;
  if (!transfer_string_list_table) return;
  for (i = 0; transfer_string_list_table[i].xml_path; i++)
  {
    /* First remove any old branches in the XML tree */

    xml_element_destroy_children_by_name(root, transfer_string_list_table[i].xml_path);

    /* Add branches corresponding to listed data */

    list = glade_xml_get_widget(glade_interface, transfer_string_list_table[i].list);
    item = gtk_container_children(GTK_CONTAINER(list));
    if (!item) continue;

    for (; item; item = item->next)
    {
      gtk_label_get(GTK_LABEL(GTK_BIN(item->data)->child), &text);

      /* Text shouldn't be freed, as it's a pointer to private data */

      node = xml_element_add(root, transfer_string_list_table[i].xml_path);
      xml_element_set_content(node, text);
    }
  }
}

/* Caller must free returned array, and all the strings in it. */
static char **
clist_entry_from_xml(TransStringCList *trans_clist, xmlNodePtr node)
{
  char *s;
  char **entry;
  int cur_field;
  xmlNodePtr nodesub;

  entry = g_new(char *, trans_clist->num_fields);
  /* For each field, find all the corresponding tags. */
  for (cur_field = 0; cur_field < trans_clist->num_fields; ++cur_field) {
    entry[cur_field] = NULL;
    for (nodesub = xml_element_find_first(node,
					  trans_clist->xml_path_fields[cur_field]); 
	 nodesub;
	 nodesub = xml_element_find_next(nodesub,
					 trans_clist->xml_path_fields[cur_field]))
      {
	if ((s = xml_element_get_content(nodesub))) 
	  {
	    /* If there are multiple copies of a field tag, we concatenate them as a space-delimited list, 
	       which is almost always what we want. */
	    /* NOTE: does this get us in trouble if the tag itself has spaces in it? */
	    if (!entry[cur_field]) entry[cur_field] = s;
	    else { entry[cur_field] = g_strjoin(" ", entry[cur_field], s, NULL); }
	  }
      }
  }
  /*
    NOTE:
    In Hans' original code, it looks like an entry gets skipped over if one of the fields is missing.
    That's not the behavior we need, so we leave it out.  Maybe we should make it a flag in TransStringCList or
    something.
    
    if (!entry[i]) continue;
  */
  return entry;
}

void
xml_clist_insert(TransStringCList *trans_clist, int row, xmlNodePtr node)
{
  GtkWidget *clist;
  char **entry;
  xmlNodePtr local_copy;
  int i;

  entry = clist_entry_from_xml(trans_clist, node);
  clist = glade_xml_get_widget(glade_interface, trans_clist->clist);
  gtk_clist_insert(GTK_CLIST(clist), row, entry);

  /* We make a copy of the node, and store it as row data.
  */
  local_copy = xmlCopyNode(node, TRUE);
  gtk_clist_set_row_data(GTK_CLIST(clist), row, local_copy);

  for (i = 0; i < trans_clist->num_fields; ++i) {
    g_free(entry[i]);
  }
  g_free(entry);
}

void
xml_clist_append(TransStringCList *trans_clist, xmlNodePtr node)
{
  GtkWidget *clist;
  char **entry;
  xmlNodePtr local_copy;
  int i, row;

  entry = clist_entry_from_xml(trans_clist, node);
  clist = glade_xml_get_widget(glade_interface, trans_clist->clist);
  row = gtk_clist_append(GTK_CLIST(clist), entry);

  /* We make a copy of the node, and store it as row data.
  */
  local_copy = xmlCopyNode(node, TRUE);
  gtk_clist_set_row_data(GTK_CLIST(clist), row, local_copy);

  for (i = 0; i < trans_clist->num_fields; ++i) {
    g_free(entry[i]);
  }
  g_free(entry);
}

void
xml_clist_set_row(TransStringCList *trans_clist, int row, xmlNodePtr node)
{
  GtkWidget *clist;
  char **entry;
  xmlNodePtr local_copy;
  int i;

  entry = clist_entry_from_xml(trans_clist, node);
  clist = glade_xml_get_widget(glade_interface, trans_clist->clist);
  for (i = 0; i < trans_clist->num_fields; ++i) {
    gtk_clist_set_text(GTK_CLIST(clist), row, i, entry[i]);
    g_free(entry[i]);
  }

  /* We make a copy of the node, and store it as row data.
  */
  local_copy = xmlCopyNode(node, TRUE);
  gtk_clist_set_row_data(GTK_CLIST(clist), row, local_copy);

  for (i = 0; i < trans_clist->num_fields; ++i) {
    g_free(entry[i]);
  }
  g_free(entry);
}

void 
xml_from_clist_row(TransStringCList *trans_clist, int row, xmlNodePtr root)
{
  GtkWidget *clist;
  int i, j;
  char *col;
  char **col_elems;
  xmlNodePtr node, node2;

  /* Enclosing element */
  node = xml_element_add(root, trans_clist->xml_path);

  clist = glade_xml_get_widget(glade_interface, trans_clist->clist);
  for (i = 0; i<trans_clist->num_fields; ++i) {
    /* If the column is empty, we don't include any xml for it. */
    /* NOTE: In Hans' original code, we skip a row if the first column is empty.  Here we do not. */
    if (!gtk_clist_get_text(GTK_CLIST(clist), row, i, &col)) continue;
    if (!strlen(col)) continue;
    col_elems = g_strsplit(col, " ", 0);
    for (j = 0; col_elems[j]; ++j) {
      node2 = xml_element_add(node, trans_clist->xml_path_fields[i]);
      xml_element_set_content(node2, col_elems[j]);
    }
    g_strfreev(col_elems);
  }
}

static void transfer_string_clist_xml_to_gui(TransTree *trans_tree, xmlNodePtr root)
{
  TransStringCList *transfer_string_clist_table;
  int i;
  xmlNodePtr node;

  transfer_string_clist_table = trans_tree->transfer_string_clist_table;
  if (!transfer_string_clist_table) return;
  for (i = 0; transfer_string_clist_table[i].xml_path; ++i)
    {
      printf("%d\n", i);
      /* Find the tag corresponding to a line in the clist. */
      for (node = xml_element_find_first(root, transfer_string_clist_table[i].xml_path); 
	   node;
	   node = xml_element_find_next(node, transfer_string_clist_table[i].xml_path))
	{
	  xml_clist_append(transfer_string_clist_table+i, node);
	}
    }
}


static void transfer_string_clist_gui_to_xml(TransTree *trans_tree, xmlNodePtr root)
{
  int i, row;
  gchar *col0;
  xmlNodePtr node;
  GtkWidget *clist;

  TransStringCList *transfer_string_clist_table;

  transfer_string_clist_table = trans_tree->transfer_string_clist_table;
  if (!transfer_string_clist_table) return;
  for (i = 0; transfer_string_clist_table[i].xml_path; i++)
    {
      /* First remove any old branches in the XML tree */
      
      xml_element_destroy_children_by_name(root, transfer_string_clist_table[i].xml_path);
      
      /* Add branches corresponding to listed data */
      
      
      clist = glade_xml_get_widget(glade_interface, transfer_string_clist_table[i].clist);
      for (row = 0; gtk_clist_get_text(GTK_CLIST(clist), row, 0, &col0); row++)
	{
	  xml_from_clist_row(transfer_string_clist_table + i, row, root);
	}
    }
}


void transfer_xml_to_gui(TransTree *trans_tree, xmlNodePtr root)
{
  transfer_string_entry_xml_to_gui(trans_tree, root);
  transfer_string_ip_xml_to_gui(trans_tree, root);
  transfer_string_list_xml_to_gui(trans_tree, root);
  transfer_string_clist_xml_to_gui(trans_tree, root);
}


void transfer_gui_to_xml(TransTree *trans_tree, xmlNodePtr root)
{
  transfer_string_entry_gui_to_xml(trans_tree, root);
  transfer_string_ip_gui_to_xml(trans_tree, root);
  transfer_string_list_gui_to_xml(trans_tree, root);
  transfer_string_clist_gui_to_xml(trans_tree, root);
}
