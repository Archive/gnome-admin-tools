#include <glade/glade.h>
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>

/* Structures for tables detailing the information types in the XML
   tree, and what widgets they correspond to. */

extern GladeXML *glade_interface;

typedef struct _TransStringEntry TransStringEntry;
typedef struct _TransStringList TransStringList;
typedef struct _TransStringCList2 TransStringCList2;
typedef struct _TransStringIPEntry TransStringIPEntry;


struct _TransStringEntry
{
  gchar *xml_path;

  gchar *editable;
  gchar *toggle;
  int unknown_verbose;      /* Whether to put <unknown> if not found in XML */
};


struct _TransStringList
{
  gchar *xml_path;           /* Path repeats for each item, forms list */

  gchar *list;
};


struct _TransStringCList2
{
  gchar *xml_path;
  gchar *xml_path_field_1;
  gchar *xml_path_field_2;

  gchar *clist;
};


struct _TransStringIPEntry
{
  gchar *xml_path;

  gchar *editable_1;
  gchar *editable_2;
  gchar *editable_3;
  gchar *editable_4;
  gchar *toggle;
};


void transfer_xml_to_gui(xmlNodePtr root);
void transfer_gui_to_xml(xmlNodePtr root);
