#include <gnome.h>
#include <gnome-xml/tree.h>


extern xmlDocPtr doc;


void
on_dns_list_select_child               (GtkList         *list,
                                        GtkWidget       *widget,
                                        gpointer         user_data);

void
on_dns_list_unselect_child             (GtkList         *list,
                                        GtkWidget       *widget,
                                        gpointer         user_data);

void
on_ip_insert_text                      (GtkEditable     *editable,
                                        gchar           *new_text,
                                        gint             new_text_length,
                                        gint            *position,
                                        gpointer         user_data);

void
on_dns_ip_activate                     (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_dns_add_clicked                     (GtkButton       *button,
                                        gpointer         user_data);

void
on_dns_remove_clicked                  (GtkButton       *button,
                                        gpointer         user_data);

void
on_wins_use_toggled                    (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_searchdomain_list_select_child      (GtkList         *list,
                                        GtkWidget       *widget,
                                        gpointer         user_data);

void
on_searchdomain_list_unselect_child    (GtkList         *list,
                                        GtkWidget       *widget,
                                        gpointer         user_data);

void
on_searchdomain_activate               (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_searchdomain_add_clicked            (GtkButton       *button,
                                        gpointer         user_data);

void
on_searchdomain_remove_clicked         (GtkButton       *button,
                                        gpointer         user_data);

void
on_statichost_list_select_row          (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_statichost_list_unselect_row        (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_statichost_add_ip_clicked           (GtkButton       *button,
                                        gpointer         user_data);

void
on_statichost_remove_ip_clicked        (GtkButton       *button,
                                        gpointer         user_data);

void
on_statichost_ip_activate              (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_statichost_set_aliases_clicked      (GtkButton       *button,
                                        gpointer         user_data);

void
on_statichost_alias_activate           (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_help_clicked                        (GtkButton       *button,
                                        gpointer         user_data);

void
on_ok_clicked                          (GtkButton       *button,
                                        gpointer         user_data);

void
on_cancel_clicked                      (GtkButton       *button,
                                        gpointer         user_data);

void
on_apply_clicked                       (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_name_resolution_admin_delete_event  (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_statichost_set_aliases_clicked      (GtkButton       *button,
                                        gpointer         user_data);

void
on_ip_delete_text                      (GtkEditable     *editable,
                                        gint             start_pos,
                                        gint             end_pos,
                                        gpointer         user_data);

void
on_ip_delete_text                      (GtkEditable     *editable,
                                        gint             start_pos,
                                        gint             end_pos,
                                        gpointer         user_data);
