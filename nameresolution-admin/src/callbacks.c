/* Copyright (C) 2000 Helix Code, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@helixcode.com>.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "callbacks.h"
#include "interface-helpers.h"
#include "xml.h"
#include "transfer.h"


GnomeHelpMenuEntry help_entry =
{
	"nameresolution-admin", "index.html"
};


GtkWidget *dns_entry_selected = 0;
GtkWidget *searchdomain_entry_selected = 0;
int statichost_row_selected = -1;


void
on_ip_insert_text (GtkEditable * editable, gchar * new_text, gint new_text_length, gint * position, gpointer user_data)
{
	int i, j;
	gchar digits[] = "0123456789";
	gchar *result = g_new (gchar, new_text_length);
	gint new_position = 0;

	for (i = 0, j = 0; i < new_text_length; i++)
	{
		if (strchr (digits, new_text[i]))
			result[j++] = new_text[i];
		else if (new_text[i] == '.')
		{
			if (user_data)
			{
				/* Pasted IP addresses will cascade down the entry box chain */

				gtk_widget_grab_focus (GTK_WIDGET (user_data));
				if (i + 1 < new_text_length)
				{
					gtk_editable_insert_text (GTK_EDITABLE (user_data), new_text + i + 1, new_text_length - (i + 1), &new_position);
					gtk_editable_set_position (GTK_EDITABLE (user_data), 3);
				}
			}
			break;
		}
	}

	gtk_signal_handler_block_by_func (GTK_OBJECT (editable), GTK_SIGNAL_FUNC (on_ip_insert_text), user_data);

	gtk_editable_insert_text (editable, result, j, position);

	gtk_signal_handler_unblock_by_func (GTK_OBJECT (editable), GTK_SIGNAL_FUNC (on_ip_insert_text), user_data);

	gtk_signal_emit_stop_by_name (GTK_OBJECT (editable), "insert_text");

	g_free (result);
}


void
on_ip_delete_text (GtkEditable * editable, gint start_pos, gint end_pos, gpointer user_data)
{
	if (!start_pos && !end_pos)
	{
		gtk_widget_grab_focus (GTK_WIDGET (user_data));
		gtk_editable_set_position (GTK_EDITABLE (user_data), 3);
	}
}


/* --- */


static void
dns_ip_add ()
{
	list_add_ip (GTK_LIST (my_get_widget ("dns_list")), my_get_widget ("dns_ip_1"), my_get_widget ("dns_ip_2"), my_get_widget ("dns_ip_3"), my_get_widget ("dns_ip_4"));
}


void
on_dns_list_select_child (GtkList * list, GtkWidget * widget, gpointer user_data)
{
	dns_entry_selected = widget;
	gtk_widget_set_sensitive (my_get_widget ("dns_remove"), TRUE);
}


void
on_dns_list_unselect_child (GtkList * list, GtkWidget * widget, gpointer user_data)
{
	dns_entry_selected = NULL;
	gtk_widget_set_sensitive (my_get_widget ("dns_remove"), FALSE);
}


void
on_dns_ip_activate (GtkEditable * editable, gpointer user_data)
{
	dns_ip_add ();
}


void
on_dns_add_clicked (GtkButton * button, gpointer user_data)
{
	dns_ip_add ();
}


void
on_dns_remove_clicked (GtkButton * button, gpointer user_data)
{
	if (dns_entry_selected)
		gtk_widget_destroy (dns_entry_selected);
}


void
on_wins_use_toggled (GtkToggleButton * togglebutton, gpointer user_data)
{
	if (gtk_toggle_button_get_active (togglebutton))
	{
		gtk_widget_set_sensitive (my_get_widget ("wins_ip_1"), TRUE);
		gtk_widget_set_sensitive (my_get_widget ("wins_ip_2"), TRUE);
		gtk_widget_set_sensitive (my_get_widget ("wins_ip_3"), TRUE);
		gtk_widget_set_sensitive (my_get_widget ("wins_ip_4"), TRUE);
	}
	else
	{
		gtk_widget_set_sensitive (my_get_widget ("wins_ip_1"), FALSE);
		gtk_widget_set_sensitive (my_get_widget ("wins_ip_2"), FALSE);
		gtk_widget_set_sensitive (my_get_widget ("wins_ip_3"), FALSE);
		gtk_widget_set_sensitive (my_get_widget ("wins_ip_4"), FALSE);
	}
}


static void
searchdomain_entry_add ()
{
	list_add_word (GTK_LIST (my_get_widget ("searchdomain_list")), my_get_widget ("searchdomain"));
}


void
on_searchdomain_list_select_child (GtkList * list, GtkWidget * widget, gpointer user_data)
{
	searchdomain_entry_selected = widget;
	gtk_widget_set_sensitive (my_get_widget ("searchdomain_remove"), TRUE);
}


void
on_searchdomain_list_unselect_child (GtkList * list, GtkWidget * widget, gpointer user_data)
{
	searchdomain_entry_selected = NULL;
	gtk_widget_set_sensitive (my_get_widget ("searchdomain_remove"), FALSE);
}


void
on_searchdomain_activate (GtkEditable * editable, gpointer user_data)
{
	searchdomain_entry_add ();
}


void
on_searchdomain_add_clicked (GtkButton * button, gpointer user_data)
{
	searchdomain_entry_add ();
}


void
on_searchdomain_remove_clicked (GtkButton * button, gpointer user_data)
{
	if (searchdomain_entry_selected)
		gtk_widget_destroy (searchdomain_entry_selected);
}


static void
statichost_ip_add ()
{
	clist_add_ip (GTK_CLIST (my_get_widget ("statichost_list")), my_get_widget ("statichost_ip_1"), my_get_widget ("statichost_ip_2"), my_get_widget ("statichost_ip_3"), my_get_widget ("statichost_ip_4"));
}


static void
statichost_set_aliases_do ()
{
	gchar *text;

	/* FIXME: Allows entering just spaces. This is not a serious problem,
	 * though, since the XML generator strips repeated blanks and
	 * ignores IPs with no aliases. */

	if (statichost_row_selected == -1)
		return;		/* Should never happen */

	text = gtk_editable_get_chars (GTK_EDITABLE (my_get_widget ("statichost_alias")), 0, -1);
	if (strlen (text))
	{
		gtk_clist_set_text (GTK_CLIST (my_get_widget ("statichost_list")), statichost_row_selected, 1, text);
		gtk_clist_unselect_row (GTK_CLIST (my_get_widget ("statichost_list")), statichost_row_selected, -1);
	}
	g_free (text);
}


void
on_statichost_list_select_row (GtkCList * clist, gint row, gint column, GdkEvent * event, gpointer user_data)
{
	gchar *row_data;
	gint position = 0;

	statichost_row_selected = row;
	gtk_widget_set_sensitive (my_get_widget ("statichost_remove_ip"), TRUE);
	gtk_widget_set_sensitive (my_get_widget ("statichost_set_aliases"), TRUE);
	gtk_widget_set_sensitive (my_get_widget ("statichost_alias"), TRUE);

	/* Load aliases into entry widget */

	gtk_clist_get_text (GTK_CLIST (my_get_widget ("statichost_list")), row, 1, &row_data);

	gtk_editable_delete_text (GTK_EDITABLE (my_get_widget ("statichost_alias")), 0, -1);
	gtk_editable_insert_text (GTK_EDITABLE (my_get_widget ("statichost_alias")), row_data, strlen (row_data), &position);
	gtk_widget_grab_focus (my_get_widget ("statichost_alias"));
	gtk_editable_set_position (GTK_EDITABLE (my_get_widget ("statichost_alias")), -1);
}


void
on_statichost_list_unselect_row (GtkCList * clist, gint row, gint column, GdkEvent * event, gpointer user_data)
{
	statichost_row_selected = -1;
	gtk_widget_set_sensitive (my_get_widget ("statichost_remove_ip"), FALSE);
	gtk_widget_set_sensitive (my_get_widget ("statichost_set_aliases"), FALSE);
	gtk_widget_set_sensitive (my_get_widget ("statichost_alias"), FALSE);
	gtk_editable_delete_text (GTK_EDITABLE (my_get_widget ("statichost_alias")), 0, -1);

	gtk_widget_grab_focus (my_get_widget ("statichost_ip_1"));
}


void
on_statichost_add_ip_clicked (GtkButton * button, gpointer user_data)
{
	statichost_ip_add ();
}


void
on_statichost_remove_ip_clicked (GtkButton * button, gpointer user_data)
{
	if (statichost_row_selected != -1)
		gtk_clist_remove (GTK_CLIST (my_get_widget ("statichost_list")), statichost_row_selected);
}


void
on_statichost_ip_activate (GtkEditable * editable, gpointer user_data)
{
	statichost_ip_add ();
}


void
on_statichost_set_aliases_clicked (GtkButton * button, gpointer user_data)
{
	statichost_set_aliases_do ();
}


void
on_statichost_alias_activate (GtkEditable * editable, gpointer user_data)
{
	statichost_set_aliases_do ();
}

/* --- */

void
on_help_clicked (GtkButton * button, gpointer user_data)
{
	gnome_help_display(NULL, &help_entry);
}


void
on_ok_clicked (GtkButton * button, gpointer user_data)
{
	transfer_gui_to_xml (xml_doc_get_root (doc));
	xml_doc_write_to_backend (doc, "nameresolution-conf");
	gtk_main_quit ();
}


void
on_cancel_clicked (GtkButton * button, gpointer user_data)
{
	gtk_main_quit ();
}


void
on_apply_clicked (GtkButton * button, gpointer user_data)
{
	transfer_gui_to_xml (xml_doc_get_root (doc));
	xml_doc_write_to_backend (doc, "nameresolution-conf");
}


gboolean on_name_resolution_admin_delete_event (GtkWidget * widget, GdkEvent * event, gpointer user_data)
{
	gtk_main_quit ();
	return FALSE;
}
