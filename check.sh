#!/bin/sh
# Run this to build packages.

tools=`cat Packages`
toplevel=`pwd`

for i in $tools
do
  echo
  echo
  echo \|
  echo \| Checking $i distribution.
  echo \|
  echo
  echo

  cd $i
  if ! make distcheck;
  then
    echo
    echo \|
    echo \| $i failed distcheck!
    echo \|
    echo
    exit
  fi
  cd $toplevel
done

echo
echo \|
echo \| All tools passed distcheck.
echo \|
echo
