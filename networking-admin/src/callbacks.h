#include <gnome.h>
#include <gnome-xml/tree.h>


extern xmlDocPtr doc;


void
on_ip_insert_text                      (GtkEditable     *editable,
                                        gchar           *new_text,
                                        gint             new_text_length,
                                        gint            *position,
                                        gpointer         user_data);

void
on_help_clicked                        (GtkButton       *button,
                                        gpointer         user_data);

void
on_ok_clicked                          (GtkButton       *button,
                                        gpointer         user_data);

void
on_cancel_clicked                      (GtkButton       *button,
                                        gpointer         user_data);

void
on_apply_clicked                       (GtkButton       *button,
                                        gpointer         user_data);

void
on_ip_delete_text                      (GtkEditable     *editable,
                                        gint             start_pos,
                                        gint             end_pos,
                                        gpointer         user_data);

void
on_dhcp_toggled                        (GtkWidget	*widget,
					gpointer	 data);

void
on_ip_activate			       (GtkEditable	*editable,
					gpointer	 user_data);

void
on_ip_focus			       (GtkWidget	*widget,
					GdkEventFocus	*event,
					gpointer	 user_data);

