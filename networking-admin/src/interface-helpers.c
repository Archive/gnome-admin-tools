/* Copyright (C) 2000 Helix Code, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@helixcode.com>.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "transfer.h"
#include "interface-helpers.h"


GtkWidget *
my_get_widget (gchar * name)
{
	return (glade_xml_get_widget (glade_interface, name));
}

int
ip_first_entry_is_valid (GtkEditable * ip_entry)
{
	gchar *ip;

	ip = gtk_editable_get_chars (GTK_EDITABLE (ip_entry), 0, -1);

	if (strtoul (ip, 0, 10) == 0 || strtoul (ip, 0, 10) > 254)
	{
		gtk_widget_grab_focus (GTK_WIDGET (ip_entry));
		gtk_editable_select_region (GTK_EDITABLE (ip_entry), 0, -1);
		return (FALSE);
	}

	return (TRUE);
}


int
ip_entry_is_valid (GtkEditable * ip_entry)
{
	gchar *ip;

	ip = gtk_editable_get_chars (GTK_EDITABLE (ip_entry), 0, -1);

	if (strtoul (ip, 0, 10) > 254)
	{
		gtk_widget_grab_focus (GTK_WIDGET (ip_entry));
		gtk_editable_select_region (GTK_EDITABLE (ip_entry), 0, -1);
		return (FALSE);
	}

	return (TRUE);
}

