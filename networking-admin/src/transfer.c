/* Copyright (C) 2000 Helix Code, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@helixcode.com>.
 */

/* Functions for transferring information between XML tree and UI */

#include <gnome.h>
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>
#include <glade/glade.h>
#include "interface-helpers.h"
#include "xml.h"
#include "transfer.h"


GladeXML *glade_interface;


TransStringIPEntry transfer_string_ip_table[] = {
	{"hostip", "host_ip_1", "host_ip_2", "host_ip_3", "host_ip_4", NULL}
	,
	{"netmask", "netmask_ip_1", "netmask_ip_2", "netmask_ip_3", "netmask_ip_4", NULL}
	,
	{"gateway", "gateway_ip_1", "gateway_ip_2", "gateway_ip_3", "gateway_ip_4", NULL}
	,
	{0, 0, 0, 0, 0, 0}
};


TransStringOptionMenu transfer_string_option_menu_table[] = {
	{"primaryinterface", "interface", TRUE}
	,
	{"knowninterface", "interface", FALSE}
	,
	{0, 0}
};


TransBooleanToggleButton transfer_boolean_toggle_button_table[] = {
	{"enabled", "state", "onboot", "yes", "no"}
	,
	{0, 0, 0, 0}
};

TransBooleanMethod transfer_boolean_method_table[] = {
	{"method", "dhcp", "static", "dhcp" }
	,
	{0, 0}
};

static void
transfer_boolean_toggle_button_xml_to_gui (xmlNodePtr root)
{
	int i;
	xmlNodePtr node;
	char *s;
	GtkWidget *toggle;

	for (i = 0; transfer_boolean_toggle_button_table[i].xml_path; i++)
	{
		toggle = my_get_widget (transfer_boolean_toggle_button_table[i].toggle);

		for (node = xml_element_find_first (root, transfer_boolean_toggle_button_table[i].xml_path); node; node = xml_element_find_next (node, transfer_boolean_toggle_button_table[i].xml_path))
		{
			if ((s = xml_element_get_attribute (node, transfer_boolean_toggle_button_table[i].attribute)))
			{
				if (strchr ("yYtT", s[0]))	/* Yes, true */
					gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (toggle), TRUE);
				else
					gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (toggle), FALSE);

				free (s);
			}
		}
	}
}


static void
transfer_boolean_toggle_button_gui_to_xml (xmlNodePtr root)
{
	int i;
	xmlNodePtr node;

	for (i = 0; transfer_boolean_toggle_button_table[i].xml_path; i++)
	{
		node = xml_element_find_first (root, transfer_boolean_toggle_button_table[i].xml_path);
		if (!node)
			node = xml_element_add (root, transfer_boolean_toggle_button_table[i].xml_path);

		xml_element_set_attribute (node, transfer_boolean_toggle_button_table[i].attribute, gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (my_get_widget (transfer_boolean_toggle_button_table[i].toggle))) ? transfer_boolean_toggle_button_table[i].positive : transfer_boolean_toggle_button_table[i].negative);
	}
}


static void
transfer_string_option_menu_xml_to_gui (xmlNodePtr root)
{
	int i;
	xmlNodePtr node;
	char *s;
	GtkWidget *optionmenu, *menu, *item;

	for (i = 0; transfer_string_option_menu_table[i].xml_path; i++)
	{
		optionmenu = my_get_widget (transfer_string_option_menu_table[i].optionmenu);

		for (node = xml_element_find_first (root, transfer_string_option_menu_table[i].xml_path); node; node = xml_element_find_next (node, transfer_string_option_menu_table[i].xml_path))
		{
			if ((s = xml_element_get_content (node)))
			{
				menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (optionmenu));
				if (!menu)
				{
					menu = gtk_menu_new ();
					gtk_option_menu_set_menu (GTK_OPTION_MENU (optionmenu), menu);
				}

				item = gtk_menu_item_new_with_label (s);
				gtk_widget_show (item);
				gtk_menu_append (GTK_MENU (menu), item);

				if (transfer_string_option_menu_table[i].select_default)
				{
					gtk_menu_item_select (GTK_MENU_ITEM (item));
					gtk_option_menu_set_history (GTK_OPTION_MENU (optionmenu), -1);
				}

				free (s);
			}
		}
	}
}


static void
transfer_string_option_menu_gui_to_xml (xmlNodePtr root)
{
	int i;
	xmlNodePtr node;
	GtkWidget *option_menu, *label;
	gchar *content;

	for (i = 0; transfer_string_option_menu_table[i].xml_path && transfer_string_option_menu_table[i].select_default; i++)
	{
		node = xml_element_find_first (root, transfer_string_option_menu_table[i].xml_path);
		if (!node)
			node = xml_element_add (root, transfer_string_option_menu_table[i].xml_path);

		option_menu = my_get_widget (transfer_string_option_menu_table[i].optionmenu);
		if (!option_menu)
			continue;

		label = GTK_BIN (option_menu)->child;
		if (!label)
			continue;

		gtk_label_get (GTK_LABEL (label), &content);
		xml_element_set_content (node, content);
	}
}


static void
transfer_string_ip_xml_to_gui (xmlNodePtr root)
{
	int i;
	xmlNodePtr node;
	char *s;
	gchar **ip;

	for (i = 0; transfer_string_ip_table[i].xml_path; i++)
	{
		node = xml_element_find_first (root, transfer_string_ip_table[i].xml_path);

		if (node && (s = xml_element_get_content (node)))
		{
			ip = g_strsplit (s, ".", 0);

			if (ip[0] && ip[1] && ip[2] && ip[3])
			{
				gtk_entry_set_text (GTK_ENTRY (my_get_widget (transfer_string_ip_table[i].editable_1)), ip[0]);
				gtk_entry_set_text (GTK_ENTRY (my_get_widget (transfer_string_ip_table[i].editable_2)), ip[1]);
				gtk_entry_set_text (GTK_ENTRY (my_get_widget (transfer_string_ip_table[i].editable_3)), ip[2]);
				gtk_entry_set_text (GTK_ENTRY (my_get_widget (transfer_string_ip_table[i].editable_4)), ip[3]);

				if (transfer_string_ip_table[i].toggle)
					gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (my_get_widget (transfer_string_ip_table[i].toggle)), TRUE);
			}

			g_strfreev (ip);
			free (s);
		}
	}
}


static void
transfer_string_ip_gui_to_xml (xmlNodePtr root)
{
	int i;
	xmlNodePtr node;
	gchar *content;
	gchar **ip;

	for (i = 0; transfer_string_ip_table[i].xml_path; i++)
	{
		node = xml_element_find_first (root, transfer_string_ip_table[i].xml_path);
		if (node)
			xml_element_destroy (node);

		if (transfer_string_ip_table[i].toggle && !gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (my_get_widget (transfer_string_ip_table[i].toggle))))
			continue;

		ip = calloc (5, sizeof (gchar *));
		ip[4] = 0;

		ip[0] = gtk_editable_get_chars (GTK_EDITABLE (my_get_widget (transfer_string_ip_table[i].editable_1)), 0, -1);
		ip[1] = gtk_editable_get_chars (GTK_EDITABLE (my_get_widget (transfer_string_ip_table[i].editable_2)), 0, -1);
		ip[2] = gtk_editable_get_chars (GTK_EDITABLE (my_get_widget (transfer_string_ip_table[i].editable_3)), 0, -1);
		ip[3] = gtk_editable_get_chars (GTK_EDITABLE (my_get_widget (transfer_string_ip_table[i].editable_4)), 0, -1);

		if (strlen (ip[0]) && strlen (ip[1]) && strlen (ip[2]) && strlen (ip[3]))
		{
			content = g_strjoin (".", ip[0], ip[1], ip[2], ip[3], NULL);
			node = xml_element_add (root, transfer_string_ip_table[i].xml_path);
			xml_element_set_content (node, content);
			g_free (content);
		}

		g_strfreev (ip);
	}
}


static void
transfer_boolean_method_xml_to_gui (xmlNodePtr root)
{
	int i;
	xmlNodePtr node;
	char *s;
	GtkWidget *toggle;

	for (i = 0; transfer_boolean_method_table[i].xml_path; i++)
	{
		toggle = my_get_widget (transfer_boolean_method_table[i].optionmethod);

		for (node = xml_element_find_first (root, transfer_boolean_method_table[i].xml_path); node; node = xml_element_find_next (node, transfer_boolean_method_table[i].xml_path))
		{
			if ((s = xml_element_get_content (node)))
			{
				if (strchr ("dD", s[0]))	/* DHCP */
					gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (toggle),
						TRUE);
				else
					gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (toggle),
						FALSE);

				free (s);
			}
		}
	}
}

static void
transfer_boolean_method_gui_to_xml (xmlNodePtr root)
{
	int i;
	xmlNodePtr node;

	for (i = 0; transfer_boolean_method_table[i].xml_path; i++)
	{
		node = xml_element_find_first (root,
			transfer_boolean_method_table[i].xml_path);
		if (node)
			xml_element_destroy (node);

		node = xml_element_add (root,
			transfer_boolean_method_table[i].xml_path);
		
		xml_element_set_content (node,
			gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (my_get_widget
				(transfer_boolean_method_table[i].optionmethod))) ? 
			transfer_boolean_method_table[i].negative : 
			transfer_boolean_method_table[i].positive);
	}
}

void
transfer_xml_to_gui (xmlNodePtr root)
{
	transfer_string_ip_xml_to_gui (root);
	transfer_string_option_menu_xml_to_gui (root);
	transfer_boolean_toggle_button_xml_to_gui (root);
	transfer_boolean_method_xml_to_gui (root);
}


void
transfer_gui_to_xml (xmlNodePtr root)
{
	transfer_string_ip_gui_to_xml (root);
	transfer_string_option_menu_gui_to_xml (root);
	transfer_boolean_toggle_button_gui_to_xml (root);
	transfer_boolean_method_gui_to_xml (root);
}
