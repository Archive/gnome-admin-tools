/* Copyright (C) 2000 Helix Code, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@helixcode.com>.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "callbacks.h"
#include "interface-helpers.h"
#include "xml.h"
#include "transfer.h"


GnomeHelpMenuEntry help_entry =
{
	"networking-admin", "index.html"
};


void
on_ip_insert_text (GtkEditable * editable, gchar * new_text, gint new_text_length, gint * position, gpointer user_data)
{
	int i, j;
	gchar digits[] = "0123456789";
	gchar *result = g_new (gchar, new_text_length);
	gint new_position = 0;

	for (i = 0, j = 0; i < new_text_length; i++)
	{
		if (strchr (digits, new_text[i]))
			result[j++] = new_text[i];
		else if (new_text[i] == '.')
		{
			if (new_text_length == 1)
			{
				*position = 3;
				break;
			}
				
			if (user_data)
			{
				/* Pasted IP addresses will cascade down the entry box chain */

				gtk_widget_grab_focus (GTK_WIDGET (user_data));
				if (i + 1 < new_text_length)
				{
					gtk_editable_insert_text (GTK_EDITABLE (user_data), new_text + i + 1, new_text_length - (i + 1), &new_position);
					gtk_editable_set_position (GTK_EDITABLE (user_data), 3);
				}
			}
			break;
		}
	}

	gtk_signal_handler_block_by_func (GTK_OBJECT (editable), GTK_SIGNAL_FUNC (on_ip_insert_text), user_data);

	gtk_editable_insert_text (editable, result, j, position);

	gtk_signal_handler_unblock_by_func (GTK_OBJECT (editable), GTK_SIGNAL_FUNC (on_ip_insert_text), user_data);

	gtk_signal_emit_stop_by_name (GTK_OBJECT (editable), "insert_text");

	if (*position == 3 && new_text_length == 1 && user_data)
		gtk_widget_grab_focus (GTK_WIDGET (user_data));

	g_free (result);
}


void
on_ip_delete_text (GtkEditable * editable, gint start_pos, gint end_pos, gpointer user_data)
{
	if (!start_pos && !end_pos)
	{
		gtk_widget_grab_focus (GTK_WIDGET (user_data));
		gtk_editable_set_position (GTK_EDITABLE (user_data), 3);
	}
}


void
on_ip_activate (GtkEditable * editable, gpointer user_data)
{
	gtk_widget_grab_focus (GTK_WIDGET (user_data));
	gtk_editable_set_position (GTK_EDITABLE (user_data), 0);
}

/* --- */


void
on_help_clicked (GtkButton * button, gpointer user_data)
{
	gnome_help_display(NULL, &help_entry);
}


void
on_ok_clicked (GtkButton * button, gpointer user_data)
{
	transfer_gui_to_xml (xml_doc_get_root (doc));
	xml_doc_write_to_backend (doc, "networking-conf");
	gtk_main_quit ();
}


void
on_cancel_clicked (GtkButton * button, gpointer user_data)
{
	gtk_main_quit ();
}


void
on_apply_clicked (GtkButton * button, gpointer user_data)
{
	transfer_gui_to_xml (xml_doc_get_root (doc));
	xml_doc_write_to_backend (doc, "networking-conf");
}


void
on_dhcp_toggled (GtkWidget * widget, gpointer data)
{
	gboolean val;
	gint i;
	GtkWidget *w0;
	
	if (GTK_TOGGLE_BUTTON (widget)->active) 
		val = FALSE;
	else
	{
		val = TRUE;
		w0 = my_get_widget ("host_ip_1");
		gtk_widget_grab_focus (GTK_WIDGET (w0));
	}

		
	for (i = 0; transfer_string_ip_table[i].xml_path; i++)
	{
		gtk_widget_set_sensitive (my_get_widget (transfer_string_ip_table[i].editable_1), 
				val);
		gtk_widget_set_sensitive (my_get_widget (transfer_string_ip_table[i].editable_2), 
				val);
		gtk_widget_set_sensitive (my_get_widget (transfer_string_ip_table[i].editable_3), 
				val);
		gtk_widget_set_sensitive (my_get_widget (transfer_string_ip_table[i].editable_4), 
				val);
	}

}

void
on_ip_focus (GtkWidget *widget, GdkEventFocus *event, gpointer user_data)
{
	gchar *txt;

	txt = gtk_entry_get_text (GTK_ENTRY (widget));
	if (strlen(txt) == 0)
		return;
	
	if (event->in)
		gtk_entry_select_region (GTK_ENTRY (widget), 0, -1);
	else
		gtk_entry_select_region (GTK_ENTRY (widget), 0, 0);
}

