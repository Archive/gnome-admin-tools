#include <glade/glade.h>
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>

/* Structures for tables detailing the information types in the XML
   tree, and what widgets they correspond to. */

extern GladeXML *glade_interface;

typedef struct _TransStringIPEntry TransStringIPEntry;
typedef struct _TransStringOptionMenu TransStringOptionMenu;
typedef struct _TransBooleanToggleButton TransBooleanToggleButton;
typedef struct _TransBooleanMethod TransBooleanMethod;

struct _TransStringIPEntry
{
  gchar *xml_path;

  gchar *editable_1;
  gchar *editable_2;
  gchar *editable_3;
  gchar *editable_4;
  gchar *toggle;
};


struct _TransStringOptionMenu
{
  gchar *xml_path;
  
  gchar *optionmenu;
  int select_default;
};


struct _TransBooleanToggleButton
{
  gchar *xml_path;
  gchar *attribute;
  
  gchar *toggle;

  gchar *positive, *negative;  /* These apply only when creating the tag */
};

struct _TransBooleanMethod
{
  gchar *xml_path;
 
  gchar *optionmethod;

  gchar *positive, *negative;
};

extern TransStringIPEntry transfer_string_ip_table[];

void transfer_xml_to_gui(xmlNodePtr root);
void transfer_gui_to_xml(xmlNodePtr root);

