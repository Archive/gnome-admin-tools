/* -*- mode: c; style: linux -*- */

/* main.c
 * Copyright (C) 2000 Helix Code, Inc.
 *
 * Written by Bradford Hovinen (hovinen@helixcode.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>

#include "main-dialog.h"
#include "xml.h"
#include "hardware.h"

static void
apply_cb (MainDialog *dialog, gboolean apply_old) 
{
	printf ("Applying...\n");
}

int
main (int argc, char **argv) 
{
	xmlDocPtr doc;
	GList *hardware;

        bindtextdomain (PACKAGE, GNOMELOCALEDIR);
        textdomain (PACKAGE);

	gnome_init ("hardware-admin", "0.0.1", argc, argv);
	hardware_table_init ();
	glade_gnome_init ();

	doc = xml_doc_read_from_backend ("hardware-conf");
	if (!doc) g_error ("Unable to read from backend.");
	hardware = read_xml_data (doc);
	xmlFreeDoc (doc);

	main_dialog_new (hardware, apply_cb);
	gtk_main ();

	return 0;
}
