/* -*- mode: c; style: linux -*- */

/* hardware.c
 * Copyright (C) 2000 Helix Code, Inc.
 *
 * Written by Bradford Hovinen (hovinen@helixcode.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include <stdlib.h>
#include <string.h>

#include "hardware.h"
#include "xml.h"

static GTree *hardware_table;

void
hardware_table_init (void) 
{
	hardware_table = g_tree_new ((GCompareFunc) strcmp);

	g_tree_insert (hardware_table, "Serial port",
		       hardware_type_new ("Serial Ports"));
	g_tree_insert (hardware_table, "Parallel port",
		       hardware_type_new ("Parallel Ports"));
	g_tree_insert (hardware_table, "Disk controller",
		       hardware_type_new ("Disk Controllers"));
	g_tree_insert (hardware_table, "Network interface card",
		       hardware_type_new ("Network Interface Cards"));
	g_tree_insert (hardware_table, "Keyboard",
		       hardware_type_new ("Keyboards"));
}

typedef struct 
{
	GtkCTree *ctree;
	GtkCTreeNode *node;
} pair_t;

static gint
populate_cb (gchar *key, hardware_type_t *value, pair_t *pair) 
{
	gchar *text[1];

	text[0] = value->name;

	pair->node = value->tree_node = 
		gtk_ctree_insert_node (pair->ctree, 
				       NULL, pair->node, 
				       text, 0, 
				       NULL, NULL, NULL, NULL, 
				       FALSE, FALSE);

	return FALSE;
}

void
hardware_table_populate_ctree (GtkCTree *ctree) 
{
	pair_t *pair;

	pair = g_new0 (pair_t, 1);
	pair->ctree = ctree;

	g_tree_traverse (hardware_table, (GTraverseFunc) populate_cb,
			 G_IN_ORDER, pair);

	g_free (pair);
}

hardware_type_t *
hardware_type_new (gchar *name)
{
	hardware_type_t *type;

	type = g_new0 (hardware_type_t, 1);
	type->name = g_strdup (name);

	return type;
}

void
hardware_type_destroy (hardware_type_t *type) 
{
	g_free (type->name);
	g_free (type);
}

io_port_rec_t *
io_port_rec_new (gint low, gint high)
{
	io_port_rec_t *rec;

	rec = g_new0 (io_port_rec_t, 1);
	rec->low = low;
	rec->high = high;
	return rec;
}

void
io_port_rec_destroy (io_port_rec_t *rec) 
{
	g_free (rec);
}

hardware_t *
hardware_new (void) 
{
	hardware_t *hardware;

	hardware = g_new0 (hardware_t, 1);
	return hardware;
}

void
hardware_destroy (hardware_t *hardware) 
{
	g_free (hardware->id);
	g_free (hardware->name);
	g_free (hardware);
}

static gboolean
read_boolean (char *str) 
{
	if (!g_strcasecmp (str, "yes") || !g_strcasecmp (str, "true"))
		return TRUE;
	else
		return FALSE;
}

void
hardware_read (hardware_t *hardware, xmlNodePtr device_node) 
{
	xmlNodePtr node;
	xmlNodePtr type_node = NULL;
	xmlNodePtr name_node = NULL;

	hardware->id = g_strdup (xmlGetProp (device_node, "id"));

	node = device_node->childs;

	while (node) {
		if (!strcmp (node->name, "type") && !type_node)
			type_node = node;
		else if (!strcmp (node->name, "name") && !name_node)
			name_node = node;
/* FIXME: I/O ports, IRQs, DMA channels */
		node = node->next;
	}

	if (!(type_node && name_node))
		g_warning ("Badly formed XML node");

	hardware->type = g_tree_lookup (hardware_table,
					xml_element_get_content (type_node));
	hardware->name = g_strdup (xml_element_get_content (name_node));
}

xmlNodePtr
hardware_write (hardware_t *hardware) 
{
	xmlNodePtr node;

	node = xmlNewNode (NULL, "device");
	xmlNewProp (node, "id", hardware->id);
	xmlNewChild (node, NULL, "type", hardware->type->name);

/* FIXME: I/O ports, IRQs, DMA channels */

	return node;
}

GList *
read_xml_data (xmlDocPtr doc) 
{
	xmlNodePtr node;
	GList *list_head, *list_tail;
	hardware_t *hardware;

	node = xml_doc_get_root (doc)->childs;
	list_head = list_tail = NULL;

	while (node) {
		if (!strcmp (node->name, "device")) {
			hardware = hardware_new ();
			hardware_read (hardware, node);
			if (hardware) {
				list_tail = 
					g_list_append (list_tail, hardware);
				if (list_head)
					list_tail = list_tail->next;
				else
					list_head = list_tail;
			}
		}

		node = node->next;
	}

	return list_head;
}

xmlDocPtr
write_xml_data (GList *hardware) 
{
	xmlDocPtr doc;
	xmlNodePtr root;

	doc = xmlNewDoc ("1.0");
	root = xmlNewDocNode (doc, NULL, "hardware", NULL);

	while (hardware) {
		xmlAddChild (root, hardware_write (hardware->data));
		hardware = hardware->next;
	}

	xmlDocSetRootElement (doc, root);

	return doc;
}
