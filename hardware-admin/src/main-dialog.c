/* -*- mode: c; style: linux -*- */

/* main-dialog.c
 * Copyright (C) 2000 Helix Code, Inc.
 *
 * Written by Bradford Hovinen (hovinen@helixcode.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gdk-pixbuf/gdk-pixbuf.h>

#include "main-dialog.h"
#include "xml.h"
#include "util.h"

static void ok_cb                       (GtkButton       *button,
                                         MainDialog      *dialog);
static void apply_cb                    (GtkButton       *button,
                                         MainDialog      *dialog);
static void cancel_cb                   (GtkButton       *button,
                                         MainDialog      *dialog);
static void add_cb                      (GtkButton       *button,
                                         MainDialog      *dialog);
static void remove_cb                   (GtkButton       *button,
                                         MainDialog      *dialog);
static void settings_cb                 (GtkButton       *button,
                                         MainDialog      *dialog);
static void select_row_cb               (GtkCTree        *ctree, 
					 GList           *node, 
					 gint             column,
					 MainDialog      *dialog);

static void add_done_cb                 (void *creation_dialog, 
					 hardware_t *device,
					 MainDialog *dialog);
static void settings_done_cb            (void *settings_dialog, 
					 MainDialog *dialog);

static void add_hardware                (MainDialog      *dialog, 
					 hardware_t      *device);
static void update_ctree                (GtkCTree        *ctree, 
					 hardware_t      *device);
static void remove_hardware             (MainDialog      *dialog,
					 hardware_t      *device);
static void populate_dialog             (MainDialog      *dialog,
					 GList           *hardware);
static void set_buttons_sensitive       (MainDialog      *dialog, 
					 gboolean         s);

MainDialog *
main_dialog_new (GList *hardware,
		 void (*apply_callback) (MainDialog *, gboolean)) 
{
	MainDialog *dialog;
	GtkWidget *add_button;

	dialog = g_new0 (MainDialog, 1);

	dialog->dialog_data = read_glade_interface ("hardware-admin.glade");

	dialog->dialog =
		GNOME_DIALOG (glade_xml_get_widget (dialog->dialog_data, 
						    "hardware_admin"));
	dialog->hardware_tree =
		GTK_CTREE (glade_xml_get_widget (dialog->dialog_data,
						 "hardware_tree"));
 	dialog->remove_button =
		GTK_BUTTON (glade_xml_get_widget (dialog->dialog_data,
						  "remove_button"));
 	dialog->settings_button =
		GTK_BUTTON (glade_xml_get_widget (dialog->dialog_data,
						  "settings_button"));

	dialog->apply_cb = apply_callback;
	dialog->hardware = hardware;

	add_button = glade_xml_get_widget (dialog->dialog_data,
					   "add_button");

	gnome_dialog_button_connect (dialog->dialog, 0,
				     GTK_SIGNAL_FUNC (ok_cb), dialog);
	gnome_dialog_button_connect (dialog->dialog, 1,
				     GTK_SIGNAL_FUNC (apply_cb), dialog);
	gnome_dialog_button_connect (dialog->dialog, 2,
				     GTK_SIGNAL_FUNC (cancel_cb), dialog);

	gtk_signal_connect (GTK_OBJECT (dialog->hardware_tree),
			    "tree-select-row", select_row_cb, dialog);
	gtk_signal_connect (GTK_OBJECT (add_button),
			    "clicked", add_cb, dialog);
	gtk_signal_connect (GTK_OBJECT (dialog->remove_button),
			    "clicked", remove_cb, dialog);
	gtk_signal_connect (GTK_OBJECT (dialog->settings_button),
			    "clicked", settings_cb, dialog);

	hardware_table_populate_ctree (dialog->hardware_tree);
	populate_dialog (dialog, hardware);

	return dialog;
}

void 
main_dialog_destroy (MainDialog *dialog) 
{
	gnome_dialog_close (dialog->dialog);
	gtk_object_destroy (GTK_OBJECT (dialog->dialog_data));
	g_free (dialog);
}

static void
ok_cb (GtkButton *button, MainDialog *dialog)
{
	dialog->apply_cb (dialog, FALSE);
	gtk_main_quit ();   /* FIXME */
}


static void
apply_cb (GtkButton *button, MainDialog *dialog)
{
	dialog->apply_cb (dialog, FALSE);
}


static void
cancel_cb (GtkButton *button, MainDialog *dialog)
{
	dialog->apply_cb (dialog, TRUE);
	gtk_main_quit ();   /* FIXME */
}


static void
add_cb (GtkButton *button, MainDialog *dialog) 
{
/* 	creation_dialog_new (CREATION_CALLBACK (add_done_cb), dialog); */
}

static void
remove_cb (GtkButton *button, MainDialog *dialog)
{
	remove_hardware (dialog, dialog->current_device);
}

static void
settings_cb (GtkButton *button, MainDialog *dialog) 
{
/* 	settings_dialog_new (dialog->current_device,  */
/* 			     SETTINGS_CALLBACK (settings_done_cb), dialog); */
}

static void
select_row_cb (GtkCTree *ctree, GList *node, gint column, 
	       MainDialog *dialog) 
{
 	dialog->current_device = GTK_CTREE_ROW (node)->row.data;
}

static void
add_done_cb (void *creation_dialog, hardware_t *device,
	     MainDialog *dialog) 
{
	dialog->hardware = 
		g_list_append (dialog->hardware, device);
	add_hardware (dialog, device);
}

static void
settings_done_cb (void *settings_dialog, MainDialog *dialog) 
{
/* 	update_ctree (dialog->hardware_tree, settings_dialog->hardware); */
}

static void
add_hardware (MainDialog *dialog, hardware_t *hardware)
{
	char *cols[1];
	GtkCTreeNode *node, *parent;

	cols[0] = hardware->name;

	if (hardware->type)
		parent = hardware->type->tree_node;
	else
		parent = NULL;

	node = gtk_ctree_insert_node (dialog->hardware_tree, 
				      parent, NULL, cols, 0, 
				      NULL, NULL, NULL, NULL, TRUE, TRUE);
	GTK_CTREE_ROW (node)->row.data = hardware;

	set_buttons_sensitive (dialog, TRUE);
}

static void
update_ctree (GtkCTree *ctree, hardware_t *hardware) 
{
/* 	gint row; */

/* 	row = gtk_ctree_find_row_from_data (ctree, hardware); */
/* 	gtk_ctree_set_text (ctree, row, 1, hardware->device); */
/* 	gtk_ctree_set_text (ctree, row, 2, priority_str); */
}

static void
remove_hardware (MainDialog *dialog, hardware_t *hardware) 
{
/* 	gint row; */

/* 	row = gtk_ctree_find_row_from_data (dialog->hardware_tree, */
/* 					    hardware); */
/* 	dialog->hardwares = g_list_remove (dialog->hardwares,  */
/* 					      hardware); */
/* 	gtk_ctree_remove (dialog->hardware_tree, row); */

/* 	if (hardware == dialog->current_device) { */
/* 		if (row == dialog->hardware_tree->rows) */
/* 			row--; */

/* 		if (row >= 0) { */
/* 			gtk_ctree_select_row */
/* 				(dialog->hardware_tree, row, 0); */
/* 			dialog->current_device = gtk_ctree_get_row_data */
/* 				(dialog->hardware_tree, row); */
/* 		} else { */
/* 			set_buttons_sensitive (dialog, FALSE); */
/* 		} */
/* 	} */

	hardware_destroy (hardware);
}

static void
populate_dialog (MainDialog *dialog, GList *hardware) 
{
	GList *node;

	node = hardware;

	while (node) {
		add_hardware (dialog, node->data);
		node = node->next;
	}

/* 	if (hardwares) */
/* 		gtk_ctree_select (dialog->hardware, 0, 0); */
/* 	else */
/* 		set_buttons_sensitive (dialog, FALSE); */
}

static void
set_buttons_sensitive (MainDialog *dialog, gboolean s) 
{
	gtk_widget_set_sensitive (GTK_WIDGET (dialog->remove_button), s);
	gtk_widget_set_sensitive (GTK_WIDGET (dialog->settings_button), s);
}
