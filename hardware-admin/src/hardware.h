/* -*- mode: c; style: linux -*- */

/* hardware.h
 * Copyright (C) 2000 Helix Code, Inc.
 *
 * Written by Bradford Hovinen (hovinen@helixcode.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef __HARDWARE_H
#define __HARDWARE_H

#include <glib.h>
#include <gtk/gtk.h>
#include <tree.h>

struct _hardware_type_t
{
	char *name;
	GtkCTreeNode *tree_node;
};

typedef struct _hardware_type_t hardware_type_t;

struct _io_port_rec_t
{
	gint low;
	gint high;
};

typedef struct _io_port_rec_t io_port_rec_t;

struct _hardware_t 
{
	char *id;
	char *name;
	hardware_type_t *type;
	GList *io_ports;
	GList *interrupts;
	GList *dma;
};

typedef struct _hardware_t hardware_t;

void             hardware_table_init (void);
void             hardware_table_populate_ctree (GtkCTree *ctree);

hardware_type_t *hardware_type_new (gchar *name);
void             hardware_type_destroy (hardware_type_t *type);

io_port_rec_t   *io_port_rec_new     (gint low, gint high);
void             io_port_rec_destroy (io_port_rec_t *rec);

hardware_t      *hardware_new        (void);
void             hardware_destroy    (hardware_t *hardware);
void             hardware_read       (hardware_t *hardware,
				    xmlNodePtr swapdev_node);
xmlNodePtr       hardware_write      (hardware_t *hardware);

GList           *read_xml_data       (xmlDocPtr doc);
xmlDocPtr        write_xml_data      (GList *hardware);

#endif /* __HARDWARE_H */
