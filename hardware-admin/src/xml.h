/* -*- mode: c; style: linux -*- */

/* xml.h
 * Copyright (C) 2000 Helix Code, Inc.
 *
 * Written by Hans Petter Jansson (hpj@helixcode.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include <glib.h>
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>

xmlNodePtr xml_doc_get_root(xmlDocPtr doc);
xmlDocPtr xml_doc_read_from_backend(char *backend_name);
void xml_doc_write_to_backend(xmlDocPtr doc, char *backend_name);
xmlNodePtr xml_element_find_first(xmlNodePtr parent, char *name);
xmlNodePtr xml_element_find_next(xmlNodePtr sibling, char *name);
xmlNodePtr xml_element_add(xmlNodePtr parent, char *name);
char *xml_element_get_content(xmlNodePtr node);
void xml_element_set_content(xmlNodePtr node, char *text);
void xml_element_destroy(xmlNodePtr node);
void xml_element_destroy_children(xmlNodePtr parent);
void xml_element_destroy_children_by_name(xmlNodePtr parent, char *name);
