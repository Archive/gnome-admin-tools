/* -*- mode: c; style: linux -*- */

/* util.c
 * Copyright (C) 2000 Helix Code, Inc.
 *
 * Written by Bradford Hovinen (hovinen@helixcode.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "util.h"

GladeXML *
read_glade_interface (gchar *interface_name)
{
	gchar *path;
	GladeXML *glade_interface;
  
	path = g_concat_dir_and_file (SCRIPT_DIR, interface_name);

	glade_interface = glade_xml_new(path, NULL);
	if (!glade_interface) g_error("Can't find glade interface file.");

	g_free (path);

	return glade_interface;
}
