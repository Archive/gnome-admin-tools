#include <gnome.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "pixmaps.h"

#include "computer.xpm"
#include "folder.xpm"


GdkPixmap *computer_pixmap = NULL, *folder_pixmap = NULL;
GdkBitmap *computer_mask = NULL, *folder_mask = NULL;


void
pixmaps_init ()
{
	GdkPixbuf *pixbuf;

	if (!computer_pixmap)
	{
		pixbuf = gdk_pixbuf_new_from_xpm_data ((const char **) computer_xpm);
		gdk_pixbuf_render_pixmap_and_mask (pixbuf, &computer_pixmap, &computer_mask, 1);
	}

	if (!folder_pixmap)
	{
		pixbuf = gdk_pixbuf_new_from_xpm_data ((const char **) folder_xpm);
		gdk_pixbuf_render_pixmap_and_mask (pixbuf, &folder_pixmap, &folder_mask, 1);
	}
}
