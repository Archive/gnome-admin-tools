/* Functions for transferring information between XML tree and UI */

#include <gnome.h>
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>
#include <glade/glade.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "xml.h"
#include "transfer.h"
#include "interface-helpers.h"
#include "pixmaps.h"

GladeXML *glade_interface;


/* Requires the specific configuration of CTree we're using. This code is by no
 * means generic. */

#define IMPORTS_CTREE_COLS_NUM 7

static void
transfer_imports_ctree_xml_to_gui (xmlNodePtr root)
{
	xmlNodePtr node, n0, n1, n2, n3, n4;
	GtkCTree *ct;
	GtkCTreeNode *first = NULL, *ctn0;
	char *text[IMPORTS_CTREE_COLS_NUM], *textr;
	char *hostname;
	int i;

	/* Initialize */

	for (i = 0; i < IMPORTS_CTREE_COLS_NUM; i++)
		text[i] = NULL;
	ct = GTK_CTREE (glade_xml_get_widget (glade_interface, "imports"));

	/* Populate */

	for (node = xml_element_find_first (root, "import"); node; node = xml_element_find_next (node, "import"))
	{
		for (i = 0; i < IMPORTS_CTREE_COLS_NUM; i++)
		{
			if (text[i])
			{
				free (text[i]);
				text[i] = NULL;
			}
		}

		n0 = xml_element_find_first (node, "host");
		n1 = xml_element_find_first (node, "path");
		n2 = xml_element_find_first (node, "user");
		n3 = xml_element_find_first (node, "password");
		n4 = xml_element_find_first (node, "point");

		if (!(n0 && (hostname = xml_element_get_content (n0))))
			continue;
		if (!(n1 && (text[0] = xml_element_get_content (n1))))
		{
			free (hostname);
			continue;
		}
		if (n2)
			text[4] = xml_element_get_content (n2);
		if (n4)
			text[6] = xml_element_get_content (n4);

		/* See if we already have the host */

		ctn0 = NULL;

		if (first)
		{
			for (ctn0 = first; ctn0; ctn0 = GTK_CTREE_NODE_NEXT (ctn0))
			{
				/* We don't want to compare against shares, so we identify them
				 * by their selectability. */

				if (gtk_ctree_node_get_selectable (ct, ctn0))
					continue;
				gtk_ctree_get_node_info (ct, ctn0, &textr, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

				if (!strcasecmp (textr, hostname))
					break;
			}
		}

		if (!ctn0)
		{
			char *hosttext[IMPORTS_CTREE_COLS_NUM];

			for (i = 0; i < IMPORTS_CTREE_COLS_NUM; i++)
				hosttext[i] = NULL;
			hosttext[0] = hostname;

			ctn0 = gtk_ctree_insert_node (ct, NULL, NULL, hosttext, 0, computer_pixmap, computer_mask, computer_pixmap, computer_mask, FALSE, TRUE);

			gtk_ctree_node_set_selectable (ct, ctn0, FALSE);
			if (!first)
				first = ctn0;
		}

		ctn0 = gtk_ctree_insert_node (ct, ctn0, NULL, text, 0, folder_pixmap, folder_mask, folder_pixmap, folder_mask, TRUE, TRUE);

		gtk_ctree_node_set_row_data (ct, ctn0, node);
		set_ctree_checkmark (ct, ctn0, 5, n3 ? TRUE : FALSE);
		set_ctree_checkmark (ct, ctn0, 1, xml_element_get_state (node, "detected"));
		set_ctree_checkmark (ct, ctn0, 2, xml_element_get_state (node, "listed"));
		set_ctree_checkmark (ct, ctn0, 3, xml_element_get_state (node, "mounted"));

		free (hostname);
	}
}


static void
transfer_imports_ctree_gui_to_xml (xmlNodePtr root)
{
	GtkCTree *ct;
	GtkCTreeNode *ctn;
	xmlNodePtr node, n0;
	gchar *host;
	gchar *text;
	gboolean is_leaf;
	GdkPixmap *pixmap;
	GdkBitmap *mask;
	guint8 spacing;

	ct = GTK_CTREE (my_get_widget ("imports"));

	for (ctn = gtk_ctree_node_nth (ct, 0); ctn; ctn = GTK_CTREE_NODE_NEXT (ctn))
	{
		/* If host node, save the host string for coming entries */

		gtk_ctree_get_node_info (ct, ctn, NULL, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);
		if (!is_leaf)
		{
			if (!gtk_ctree_node_get_pixtext (ct, ctn, 0, &host, &spacing, &pixmap, &mask))
				host = NULL;

			continue;
		}

		node = gtk_ctree_node_get_row_data (ct, ctn);

		if (!host)
		{
			/* This shouldn't happen, theoretically speaking */

			xml_element_destroy (node);
			continue;
		}

		/* Get strings */

		gtk_ctree_node_get_pixtext (ct, ctn, 0, &text, &spacing, &pixmap, &mask);
		xml_element_add_with_content (node, "path", text);

		if (gtk_ctree_node_get_text (ct, ctn, 4, &text) && strlen (text))
			xml_element_add_with_content (node, "user", text);
		else
			xml_element_destroy_children_by_name (node, "user");

		n0 = xml_element_find_first (node, "password");
		if (n0)
		{
			text = xml_element_get_content (n0);
			if (!text || !strlen (text))
				xml_element_destroy (n0);
		}

		if (gtk_ctree_node_get_text (ct, ctn, 6, &text) && strlen (text))
			xml_element_add_with_content (node, "point", text);
		else
			xml_element_destroy_children_by_name (node, "point");

		/* Get booleans */

		xml_element_set_state (node, "listed", get_ctree_checkmark (ct, ctn, 2));
		xml_element_set_state (node, "mounted", get_ctree_checkmark (ct, ctn, 3));
	}
}


/* SMB exports */

#define SMB_EXPORTS_CLIST_COLS_NUM 7

static void
transfer_smb_exports_clist_xml_to_gui (xmlNodePtr root)
{
	xmlNodePtr node, n0, n1, n2;
	GtkCList *cl;
	int row;
	char *text[SMB_EXPORTS_CLIST_COLS_NUM];
	char *s0;
	int i;

	/* Initialize */

	for (i = 0; i < SMB_EXPORTS_CLIST_COLS_NUM; i++)
		text[i] = NULL;
	cl = GTK_CLIST (glade_xml_get_widget (glade_interface, "smb_exports"));

	/* Populate */

	for (node = xml_element_find_first (root, "export"); node; node = xml_element_find_next (node, "export"))
	{
		s0 = xml_element_get_attribute (node, "type");
		if (!s0)
			continue;
		if (strcasecmp (s0, "smb"))
		{
			free (s0);
			continue;
		}
		free (s0);

		for (i = 0; i < SMB_EXPORTS_CLIST_COLS_NUM; i++)
		{
			if (text[i])
			{
				free (text[i]);
				text[i] = NULL;
			}
		}

		n0 = xml_element_find_first (node, "name");
		n1 = xml_element_find_first (node, "path");
		n2 = xml_element_find_first (node, "comment");

		if (!(n0 && (text[0] = xml_element_get_content (n0))))
			continue;
		if (!(n1 && (text[1] = xml_element_get_content (n1))))
		{
			free (text[0]);
			continue;
		}
		if (n2)
			text[6] = xml_element_get_content (n2);

		row = gtk_clist_append (cl, text);

		gtk_clist_set_row_data (cl, row, node);
		gtk_clist_set_pixtext (cl, row, 0, text[0], 0, folder_pixmap, folder_mask);
		set_clist_checkmark (cl, row, 2, xml_element_get_state (node, "enabled"));
		set_clist_checkmark (cl, row, 3, xml_element_get_state (node, "browse"));
		set_clist_checkmark (cl, row, 4, xml_element_get_state (node, "public"));
		set_clist_checkmark (cl, row, 5, xml_element_get_state (node, "write"));
	}
}


static void
transfer_smb_exports_clist_gui_to_xml (xmlNodePtr root)
{
	GtkCList *cl;
	int row;
	xmlNodePtr node;
	gchar *text;
	GdkPixmap *pixmap;
	GdkBitmap *mask;
	guint8 spacing;

	cl = GTK_CLIST (my_get_widget ("smb_exports"));

	for (row = 0;; row++)
	{
		node = gtk_clist_get_row_data (cl, row);
		if (!node)
			break;

		/* Get strings */

		gtk_clist_get_pixtext (cl, row, 0, &text, &spacing, &pixmap, &mask);
		xml_element_add_with_content (node, "name", text);

		gtk_clist_get_text (cl, row, 1, &text);
		xml_element_add_with_content (node, "path", text);

		gtk_clist_get_text (cl, row, 6, &text);
		xml_element_add_with_content (node, "comment", text);

		/* Get booleans */

		xml_element_set_state (node, "enabled", get_clist_checkmark (cl, row, 2));
		xml_element_set_state (node, "browse", get_clist_checkmark (cl, row, 3));
		xml_element_set_state (node, "public", get_clist_checkmark (cl, row, 4));
		xml_element_set_state (node, "write", get_clist_checkmark (cl, row, 5));
	}
}


/* NFS exports */


#define NFS_EXPORTS_CTREE_COLS_NUM 2

static void
transfer_nfs_exports_ctree_xml_to_gui (xmlNodePtr root)
{
	xmlNodePtr node, node2, n0;
	GtkCTree *ct;
	GtkCTreeNode *first = NULL, *ctn0, *ctn1;
	char *text[NFS_EXPORTS_CTREE_COLS_NUM], *textr;
	char *path, *s0;
	GdkPixmap *pixmap;
	GdkBitmap *mask;
	int i;

	/* Initialize */

	for (i = 0; i < NFS_EXPORTS_CTREE_COLS_NUM; i++)
		text[i] = NULL;
	ct = GTK_CTREE (glade_xml_get_widget (glade_interface, "nfs_exports"));

	/* Populate */

	for (node = xml_element_find_first (root, "export"); node; node = xml_element_find_next (node, "export"))
	{
		s0 = xml_element_get_attribute (node, "type");
		if (!s0)
			continue;
		if (strcasecmp (s0, "nfs"))
		{
			free (s0);
			continue;
		}
		free (s0);

		for (i = 0; i < NFS_EXPORTS_CTREE_COLS_NUM; i++)
		{
			if (text[i])
			{
				free (text[i]);
				text[i] = NULL;
			}
		}

		n0 = xml_element_find_first (node, "path");

		if (!(n0 && (path = xml_element_get_content (n0))))
			continue;

		/* See if we already have the path */

		ctn0 = NULL;

		if (first)
		{
			for (ctn0 = first; ctn0; ctn0 = GTK_CTREE_NODE_NEXT (ctn0))
			{
				/* We don't want to compare against hosts, so we identify them
				 * by their computer icon. */

				gtk_ctree_node_get_pixmap (ct, ctn0, 0, &pixmap, &mask);
				if (pixmap == computer_pixmap)
					continue;
				gtk_ctree_get_node_info (ct, ctn0, &textr, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

				if (!strcasecmp (textr, path))
					break;
			}
		}

		if (!ctn0)
		{
			char *sharetext[NFS_EXPORTS_CTREE_COLS_NUM];

			for (i = 0; i < NFS_EXPORTS_CTREE_COLS_NUM; i++)
				sharetext[i] = NULL;
			sharetext[0] = path;

			ctn0 = gtk_ctree_insert_node (ct, NULL, NULL, sharetext, 0, folder_pixmap, folder_mask, folder_pixmap, folder_mask, FALSE, TRUE);

			gtk_ctree_node_set_row_data (ct, ctn0, node);
			if (!first)
				first = ctn0;
		}

		for (node2 = xml_element_find_first (node, "allow"); node2; node2 = xml_element_find_next (node2, "allow"))
		{
			n0 = xml_element_find_first (node2, "pattern");
			if (!(n0 && (text[0] = xml_element_get_content (n0))))
				continue;

			ctn1 = gtk_ctree_insert_node (ct, ctn0, NULL, text, 0, computer_pixmap, computer_mask, computer_pixmap, computer_mask, TRUE, TRUE);

			gtk_ctree_node_set_row_data (ct, ctn1, node2);
			set_ctree_checkmark (ct, ctn1, 1, xml_element_get_state (node2, "write"));
		}

		free (path);
	}
}


static void
transfer_nfs_exports_ctree_gui_to_xml (xmlNodePtr root)
{
	GtkCTree *ct;
	GtkCTreeNode *ctn;
	xmlNodePtr node;
	gchar *text;
	gboolean is_leaf;
	GdkPixmap *pixmap;
	GdkBitmap *mask;
	guint8 spacing;

	ct = GTK_CTREE (my_get_widget ("nfs_exports"));

	for (ctn = gtk_ctree_node_nth (ct, 0); ctn; ctn = GTK_CTREE_NODE_NEXT (ctn))
	{
		/* If host node, save the host string for coming entries */

		gtk_ctree_get_node_info (ct, ctn, NULL, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);
		node = gtk_ctree_node_get_row_data (ct, ctn);

		if (!is_leaf)
		{
			gtk_ctree_node_get_pixtext (ct, ctn, 0, &text, &spacing, &pixmap, &mask);
			xml_element_add_with_content (node, "path", text);
		}
		else
		{
			gtk_ctree_node_get_pixtext (ct, ctn, 0, &text, &spacing, &pixmap, &mask);
			xml_element_add_with_content (node, "pattern", text);

			xml_element_set_state (node, "write", get_ctree_checkmark (ct, ctn, 1));
		}
	}
}


void
transfer_xml_to_gui (xmlNodePtr root)
{
	transfer_imports_ctree_xml_to_gui (root);
	transfer_smb_exports_clist_xml_to_gui (root);
	transfer_nfs_exports_ctree_xml_to_gui (root);
}


void
transfer_gui_to_xml (xmlNodePtr root)
{
	transfer_imports_ctree_gui_to_xml (root);
	transfer_smb_exports_clist_gui_to_xml (root);
	transfer_nfs_exports_ctree_gui_to_xml (root);
}
