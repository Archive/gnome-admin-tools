/* Copyright (C) 2000 Helix Code, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Hans Petter Jansson <hpj@helixcode.com>.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "callbacks.h"
#include "interface-helpers.h"
#include "xml.h"
#include "transfer.h"
#include "pixmaps.h"


GnomeHelpMenuEntry help_entry =
{
	"shares-admin", "index.html"
};


GtkWidget *dns_entry_selected = 0;
GtkWidget *searchdomain_entry_selected = 0;
GtkCTreeNode *imports_node_selected = NULL;
int smb_exports_row_selected = -1;
GtkCTreeNode *nfs_exports_node_selected = NULL;


/* --- Prototypes --- */


void import_dialog_prepare_new (void);
void import_dialog_affect_new (void);
void import_dialog_prepare_from_selected (void);
void import_dialog_affect_selected (void);
void smb_export_dialog_prepare_new (void);
void smb_export_dialog_affect_new (void);
void smb_export_dialog_prepare_from_selected (void);
void smb_export_dialog_affect_selected (void);
void nfs_export_dialog_prepare_new (void);
void nfs_export_dialog_affect_new (void);
void nfs_export_dialog_prepare_from_selected (void);
void nfs_export_dialog_affect_selected (void);
void nfs_client_dialog_prepare_new (void);
void nfs_client_dialog_affect_new (void);
void nfs_client_dialog_prepare_from_selected (void);
void nfs_client_dialog_affect_selected (void);


/* --- */


static char *
path_nice (char *path0)
{
	char *path1;

	/* Remove leading and trailing blanks */

	g_strstrip (path0);

	/* Ensure a leading slash */

	if (path0[0] != '/')
	{
		path1 = g_strconcat ("/", path0, NULL);
		free (path0);
		path0 = path1;
	}

	/* Ensure no trailing slash */

	if (strlen (path0) && *(path0 + strlen (path0) - 1) == '/')
		*(path0 + strlen (path0) - 1) = 0;

	return (path0);
}


/* --- */


void
on_help_clicked (GtkButton * button, gpointer data)
{
  gnome_help_display(NULL, &help_entry);
}


void
on_listed_toggled (GtkToggleButton * toggle, gpointer data)
{
	GtkCTree *ct;

	ct = GTK_CTREE (my_get_widget ("imports"));

	set_ctree_checkmark (ct, imports_node_selected, 2, gtk_toggle_button_get_active (toggle));
}


void
on_mounted_toggled (GtkToggleButton * toggle, gpointer data)
{
	GtkCTree *ct;

	ct = GTK_CTREE (my_get_widget ("imports"));

	set_ctree_checkmark (ct, imports_node_selected, 3, gtk_toggle_button_get_active (toggle));
}


void
on_imports_tree_select_row (GtkCList * clist, gint row, gint column, GdkEvent * event, gpointer user_data)
{
	if (!gtk_ctree_node_get_selectable (GTK_CTREE (clist), gtk_ctree_node_nth (GTK_CTREE (clist), row)))
		return;

	imports_node_selected = gtk_ctree_node_nth (GTK_CTREE (clist), row);

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (my_get_widget ("listed_toggle")), get_ctree_checkmark (GTK_CTREE (clist), imports_node_selected, 2));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (my_get_widget ("mounted_toggle")), get_ctree_checkmark (GTK_CTREE (clist), imports_node_selected, 3));

	gtk_widget_set_sensitive (my_get_widget ("listed_toggle"), TRUE);
	gtk_widget_set_sensitive (my_get_widget ("mounted_toggle"), TRUE);
	gtk_widget_set_sensitive (my_get_widget ("import_settings_launch"), TRUE);
}


void
on_imports_tree_unselect_row (GtkCList * clist, gint row, gint column, GdkEvent * event, gpointer user_data)
{
	imports_node_selected = NULL;
	gtk_widget_set_sensitive (my_get_widget ("listed_toggle"), FALSE);
	gtk_widget_set_sensitive (my_get_widget ("mounted_toggle"), FALSE);
	gtk_widget_set_sensitive (my_get_widget ("import_settings_launch"), FALSE);
}


void
on_smb_exports_list_select_row (GtkCList * clist, gint row, gint column, GdkEvent * event, gpointer user_data)
{
	smb_exports_row_selected = row;

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (my_get_widget ("smb_export_enabled")), get_clist_checkmark (clist, row, 2));

	gtk_widget_set_sensitive (my_get_widget ("smb_export_enabled"), TRUE);
	gtk_widget_set_sensitive (my_get_widget ("smb_export_remove"), TRUE);
	gtk_widget_set_sensitive (my_get_widget ("smb_export_settings"), TRUE);
}


void
on_smb_exports_list_unselect_row (GtkCList * clist, gint row, gint column, GdkEvent * event, gpointer user_data)
{
	smb_exports_row_selected = -1;
	gtk_widget_set_sensitive (my_get_widget ("smb_export_enabled"), FALSE);
	gtk_widget_set_sensitive (my_get_widget ("smb_export_remove"), FALSE);
	gtk_widget_set_sensitive (my_get_widget ("smb_export_settings"), FALSE);
}


void
on_smb_export_enabled_toggled (GtkToggleButton * toggle, gpointer data)
{
	GtkCList *cl;

	cl = GTK_CLIST (my_get_widget ("smb_exports"));

	set_clist_checkmark (cl, smb_exports_row_selected, 2, gtk_toggle_button_get_active (toggle));
}


void
on_nfs_exports_tree_select_row (GtkCList * clist, gint row, gint column, GdkEvent * event, gpointer user_data)
{
	gboolean is_leaf;

	if (!gtk_ctree_node_get_selectable (GTK_CTREE (clist), gtk_ctree_node_nth (GTK_CTREE (clist), row)))
		return;

	nfs_exports_node_selected = gtk_ctree_node_nth (GTK_CTREE (clist), row);

	gtk_ctree_get_node_info (GTK_CTREE (clist), nfs_exports_node_selected, NULL, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);

	if (is_leaf)
	{
		/* It's a host pattern */

		gtk_widget_set_sensitive (my_get_widget ("nfs_export_remove"), TRUE);
		gtk_widget_set_sensitive (my_get_widget ("nfs_export_settings"), TRUE);
		gtk_widget_set_sensitive (my_get_widget ("nfs_client_add"), TRUE);
		gtk_widget_set_sensitive (my_get_widget ("nfs_client_remove"), TRUE);
		gtk_widget_set_sensitive (my_get_widget ("nfs_client_settings"), TRUE);
	}
	else
	{
		/* It's an export */

		gtk_widget_set_sensitive (my_get_widget ("nfs_export_remove"), TRUE);
		gtk_widget_set_sensitive (my_get_widget ("nfs_export_settings"), TRUE);
		gtk_widget_set_sensitive (my_get_widget ("nfs_client_add"), TRUE);
		gtk_widget_set_sensitive (my_get_widget ("nfs_client_remove"), FALSE);
		gtk_widget_set_sensitive (my_get_widget ("nfs_client_settings"), FALSE);
	}
}


void
on_nfs_exports_tree_unselect_row (GtkCList * clist, gint row, gint column, GdkEvent * event, gpointer user_data)
{
	nfs_exports_node_selected = NULL;

	gtk_widget_set_sensitive (my_get_widget ("nfs_export_remove"), FALSE);
	gtk_widget_set_sensitive (my_get_widget ("nfs_export_settings"), FALSE);
	gtk_widget_set_sensitive (my_get_widget ("nfs_client_add"), FALSE);
	gtk_widget_set_sensitive (my_get_widget ("nfs_client_remove"), FALSE);
	gtk_widget_set_sensitive (my_get_widget ("nfs_client_settings"), FALSE);
}


/* --- */


void
on_ok_clicked (GtkButton * button, gpointer user_data)
{
	transfer_gui_to_xml (xml_doc_get_root (root));
	xml_doc_write_to_backend (root, "shares-conf");
	gtk_main_quit ();
}


void
on_cancel_clicked (GtkButton * button, gpointer user_data)
{
	gtk_main_quit ();
}


void
on_apply_clicked (GtkButton * button, gpointer user_data)
{
	transfer_gui_to_xml (xml_doc_get_root (root));
	xml_doc_write_to_backend (root, "shares-conf");
}


void
on_import_type_selected (GtkMenuShell * menu, gpointer data)
{
	gchar *s0;

	gtk_label_get (GTK_LABEL (GTK_BIN (my_get_widget ("import_type"))->child), &s0);
	if (strstr (s0, "NFS"))
	{
		gtk_widget_set_sensitive (my_get_widget ("import_user"), FALSE);
		gtk_widget_set_sensitive (my_get_widget ("import_password"), FALSE);
	}
	else
	{
		gtk_widget_set_sensitive (my_get_widget ("import_user"), TRUE);
		gtk_widget_set_sensitive (my_get_widget ("import_password"), TRUE);
	}
}


void
import_dialog_prepare_from_selected ()
{
	xmlNodePtr node = NULL, n0;
	GtkCTree *ctree;
	GtkCTreeNode *ctn;
	GList *glist = NULL;
	gboolean is_leaf;
	gchar *text;

	ctree = GTK_CTREE (my_get_widget ("imports"));

	if (imports_node_selected)
		node = gtk_ctree_node_get_row_data (ctree, imports_node_selected);

	if (node)
	{
		/* Get type from XML */

		gtk_option_menu_set_history (GTK_OPTION_MENU (my_get_widget ("import_type")), 0);
		gtk_widget_set_sensitive (my_get_widget ("import_user"), TRUE);
		gtk_widget_set_sensitive (my_get_widget ("import_password"), TRUE);

		text = xml_element_get_attribute (node, "type");
		if (text)
		{
			if (!strcasecmp (text, "nfs"))
			{
				gtk_option_menu_set_history (GTK_OPTION_MENU (my_get_widget ("import_type")), 1);
				gtk_widget_set_sensitive (my_get_widget ("import_user"), FALSE);
				gtk_widget_set_sensitive (my_get_widget ("import_password"), FALSE);
			}

			free (text);
		}

		/* Round up the known hosts and add them to the option list of
		 * the host combo */

		for (ctn = gtk_ctree_node_nth (ctree, 0); ctn; ctn = GTK_CTREE_NODE_NEXT (ctn))
		{
			gtk_ctree_get_node_info (ctree, ctn, NULL, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);
			if (is_leaf)
				continue;	/* Skip import nodes */

			gtk_ctree_get_node_info (ctree, ctn, &text, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
			if (text)
				glist = g_list_append (glist, text);
		}

		if (glist)
			gtk_combo_set_popdown_strings (GTK_COMBO (my_get_widget ("import_host")), glist);

		/* Set the active hostname from the currently selected item */

		for (ctn = imports_node_selected; ctn; ctn = GTK_CTREE_NODE_PREV (ctn))
		{
			gtk_ctree_get_node_info (ctree, ctn, NULL, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);
			if (!is_leaf)
				break;	/* Found parent node */
		}

		if (ctn)
		{
			gtk_ctree_get_node_info (ctree, ctn, &text, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
			if (text)
				gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (my_get_widget ("import_host"))->entry), text);
		}

		/* Get path from ctree */

		text = NULL;
		gtk_ctree_get_node_info (ctree, imports_node_selected, &text, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
		if (text)
			gtk_entry_set_text (GTK_ENTRY (my_get_widget ("import_path")), text);
		else
			gtk_entry_set_text (GTK_ENTRY (my_get_widget ("import_path")), "");

		/* Get user from ctree */

		text = NULL;
		gtk_ctree_node_get_text (ctree, imports_node_selected, 4, &text);
		if (text)
			gtk_entry_set_text (GTK_ENTRY (my_get_widget ("import_user")), text);
		else
			gtk_entry_set_text (GTK_ENTRY (my_get_widget ("import_user")), "");

		/* Get password from XML */

		n0 = xml_element_find_first (node, "password");
		if (n0 && (text = xml_element_get_content (n0)))
		{
			gtk_entry_set_text (GTK_ENTRY (my_get_widget ("import_password")), text);
			free (text);
		}
		else
			gtk_entry_set_text (GTK_ENTRY (my_get_widget ("import_password")), "");

		/* Get mount point from ctree */

		text = NULL;
		gtk_editable_delete_text (GTK_EDITABLE (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (my_get_widget ("import_point")))), 0, -1);
		gtk_ctree_node_get_text (ctree, imports_node_selected, 6, &text);
		if (text)
			gtk_entry_set_text (GTK_ENTRY (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (my_get_widget ("import_point")))), text);
		else
			gtk_entry_set_text (GTK_ENTRY (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (my_get_widget ("import_point")))), "");
	}
}


void
import_dialog_affect_selected ()
{
	xmlNodePtr node = NULL;
	GtkCTree *ctree;
	GtkCTreeNode *ctn;
	gboolean is_leaf;
	gchar *text, *s0, *s1;
	gchar *textrow[32];
	int i;

	ctree = GTK_CTREE (my_get_widget ("imports"));

	if (imports_node_selected)
		node = gtk_ctree_node_get_row_data (ctree, imports_node_selected);

	if (!node)
		return;		/* Should never happen */

	/* Check if the host has changed. If it has, we need to move the ctree
	 * node. */

	s0 = gtk_entry_get_text (GTK_ENTRY (GTK_COMBO (my_get_widget ("import_host"))->entry));

	for (ctn = imports_node_selected; ctn; ctn = GTK_CTREE_NODE_PREV (ctn))
	{
		gtk_ctree_get_node_info (ctree, ctn, NULL, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);
		if (!is_leaf)
			break;	/* Found parent node */
	}

	if (ctn)
	{
		gtk_ctree_get_node_info (ctree, ctn, &s1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

		if (s0 && s1)
		{
			if (strcasecmp (s0, s1))
			{
				GtkCTreeNode *oldhost = ctn;

				/* We need to move. First, see if the target host is already in our
				 * tree */

				for (ctn = gtk_ctree_node_nth (ctree, 0); ctn; ctn = GTK_CTREE_NODE_NEXT (ctn))
				{
					gtk_ctree_get_node_info (ctree, ctn, NULL, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);
					if (is_leaf)
						continue;	/* Skip import nodes */

					gtk_ctree_get_node_info (ctree, ctn, &text, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
					if (text && !strcasecmp (text, s0))
						break;	/* Found it */
				}

				for (i = 0; i < 32; i++)
					textrow[i] = NULL;

				if (!ctn)
				{
					/* We didn't find a matching host entry; create it */

					textrow[0] = s0;
					ctn = gtk_ctree_insert_node (ctree, NULL, NULL, textrow, 0, computer_pixmap, computer_mask, computer_pixmap, computer_mask, FALSE, TRUE);

					gtk_ctree_node_set_selectable (ctree, ctn, FALSE);
				}

				gtk_ctree_move (ctree, imports_node_selected, ctn, GTK_CTREE_NODE_NEXT (ctn));

				/* Check if old parent host is out of children; remove it if so */

				ctn = GTK_CTREE_NODE_NEXT (oldhost);
				if (!ctn)
					gtk_ctree_remove_node (ctree, oldhost);
				else
				{
					gtk_ctree_get_node_info (ctree, ctn, NULL, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);
					if (!is_leaf)
						gtk_ctree_remove_node (ctree, oldhost);
				}
			}
		}
	}

	/* Set type in XML [and ctree] */

	gtk_label_get (GTK_LABEL (GTK_BIN (my_get_widget ("import_type"))->child), &s0);
	if (strstr (s0, "NFS"))
		xml_element_set_attribute (node, "type", "nfs");
	else
		xml_element_set_attribute (node, "type", "smb");

	/* Set path in ctree */

	s0 = path_nice (strdup (gtk_entry_get_text (GTK_ENTRY (my_get_widget ("import_path")))));

	gtk_ctree_node_set_pixtext (ctree, imports_node_selected, 0, s0, 0, folder_pixmap, folder_mask);

	free (s0);

	/* Set user in ctree */

	gtk_ctree_node_set_text (ctree, imports_node_selected, 4, gtk_entry_get_text (GTK_ENTRY (my_get_widget ("import_user"))));

	/* Set password in XML */

	s0 = gtk_entry_get_text (GTK_ENTRY (my_get_widget ("import_password")));
	xml_element_add_with_content (node, "password", s0);
	set_ctree_checkmark (ctree, imports_node_selected, 5, strlen (s0) ? TRUE : FALSE);

	/* Set mount point in ctree */

	s0 = path_nice (strdup (gtk_entry_get_text (GTK_ENTRY (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (my_get_widget ("import_point")))))));
	gtk_ctree_node_set_text (ctree, imports_node_selected, 6, s0);
	free (s0);
}


void
on_import_settings_launched (GtkButton * button, gpointer data)
{
	GnomeDialog *dialog;

	import_dialog_prepare_from_selected ();
	dialog = GNOME_DIALOG (my_get_widget ("import_settings_dialog"));

	switch (gnome_dialog_run_and_close (dialog))
	{
		case 0:
			import_dialog_affect_selected ();
			break;

		case 1:
		case -1:
		default:
			break;
	}
}


void
import_dialog_prepare_new ()
{
	xmlNodePtr node = NULL;
	GtkCTree *ctree;
	GtkCTreeNode *ctn;
	GList *glist = NULL;
	gboolean is_leaf;
	gchar *text;

	ctree = GTK_CTREE (my_get_widget ("imports"));

	if (imports_node_selected)
		node = gtk_ctree_node_get_row_data (ctree, imports_node_selected);

	/* Set default type */

	gtk_option_menu_set_history (GTK_OPTION_MENU (my_get_widget ("import_type")), 0);	/* SMB */

	/* Round up the known hosts and add them to the option list of
	 * the host combo */

	for (ctn = gtk_ctree_node_nth (ctree, 0); ctn; ctn = GTK_CTREE_NODE_NEXT (ctn))
	{
		gtk_ctree_get_node_info (ctree, ctn, NULL, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);
		if (is_leaf)
			continue;	/* Skip import nodes */

		gtk_ctree_get_node_info (ctree, ctn, &text, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
		if (text)
			glist = g_list_append (glist, text);
	}

	if (glist)
		gtk_combo_set_popdown_strings (GTK_COMBO (my_get_widget ("import_host")), glist);

	/* Set selected host to none */

	gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (my_get_widget ("import_host"))->entry), "");

	/* Set path to none */

	gtk_entry_set_text (GTK_ENTRY (my_get_widget ("import_path")), "");

	/* Set user to none */

	gtk_entry_set_text (GTK_ENTRY (my_get_widget ("import_user")), "");

	/* Set password to none */

	gtk_entry_set_text (GTK_ENTRY (my_get_widget ("import_password")), "");

	/* Set mount point to none */

	gtk_editable_delete_text (GTK_EDITABLE (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (my_get_widget ("import_point")))), 0, -1);
}


void
import_dialog_affect_new ()
{
	xmlNodePtr node = NULL;
	GtkCTree *ctree;
	GtkCTreeNode *ctn;
	gboolean is_leaf;
	gchar *text, *s0;
	gchar *textrow[32];
	int i;

	ctree = GTK_CTREE (my_get_widget ("imports"));

	node = xml_element_add (xml_doc_get_root (root), "import");
	if (!node)
		return;		/* Should never happen */

	/* Set type in XML */

	gtk_label_get (GTK_LABEL (GTK_BIN (my_get_widget ("import_type"))->child), &s0);
	if (strstr (s0, "NFS"))
		xml_element_set_attribute (node, "type", "nfs");
	else
		xml_element_set_attribute (node, "type", "smb");

	/* Find or make a suitable host node to attach to */

	s0 = gtk_entry_get_text (GTK_ENTRY (GTK_COMBO (my_get_widget ("import_host"))->entry));

	/* First, see if the target host is already in our tree */

	for (ctn = gtk_ctree_node_nth (ctree, 0); ctn; ctn = GTK_CTREE_NODE_NEXT (ctn))
	{
		gtk_ctree_get_node_info (ctree, ctn, NULL, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);
		if (is_leaf)
			continue;	/* Skip import nodes */

		gtk_ctree_get_node_info (ctree, ctn, &text, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
		if (text && !strcasecmp (text, s0))
			break;	/* Found it */
	}

	for (i = 0; i < 32; i++)
		textrow[i] = NULL;

	if (!ctn)
	{
		/* We didn't find a matching host entry; create it */

		textrow[0] = s0;
		ctn = gtk_ctree_insert_node (ctree, NULL, NULL, textrow, 0, computer_pixmap, computer_mask, computer_pixmap, computer_mask, FALSE, TRUE);

		gtk_ctree_node_set_selectable (ctree, ctn, FALSE);
	}

	/* Pull strings from dialog */

	textrow[0] = path_nice (strdup (gtk_entry_get_text (GTK_ENTRY (my_get_widget ("import_path")))));
	textrow[4] = gtk_entry_get_text (GTK_ENTRY (my_get_widget ("import_user")));
	textrow[6] = path_nice (strdup (gtk_entry_get_text (GTK_ENTRY (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (my_get_widget ("import_point")))))));

	/* Create the import's ctree node */

	ctn = gtk_ctree_insert_node (ctree, ctn, NULL, textrow, 0, folder_pixmap, folder_mask, folder_pixmap, folder_mask, TRUE, TRUE);

	set_ctree_checkmark (ctree, ctn, 5, strlen (gtk_entry_get_text (GTK_ENTRY (my_get_widget ("import_password")))) ? TRUE : FALSE);
	set_ctree_checkmark (ctree, ctn, 1, FALSE);
	set_ctree_checkmark (ctree, ctn, 2, TRUE);
	set_ctree_checkmark (ctree, ctn, 3, TRUE);

	/* Set host in XML */

	xml_element_add_with_content (node, "host", gtk_entry_get_text (GTK_ENTRY (GTK_COMBO (my_get_widget ("import_host"))->entry)));
	xml_element_add_with_content (node, "path", textrow[0]);
	xml_element_add_with_content (node, "user", textrow[4]);
	xml_element_add_with_content (node, "password", gtk_entry_get_text (GTK_ENTRY (my_get_widget ("import_password"))));

	/* Attach XML node to ctree node */

	gtk_ctree_node_set_row_data (ctree, ctn, node);

	free (textrow[0]);
	free (textrow[6]);
}


void
on_import_new_launched (GtkButton * button, gpointer data)
{
	GnomeDialog *dialog;

	import_dialog_prepare_new ();
	dialog = GNOME_DIALOG (my_get_widget ("import_settings_dialog"));

	switch (gnome_dialog_run_and_close (dialog))
	{
		case 0:
			import_dialog_affect_new ();
			break;

		case 1:
		case -1:
		default:
			break;
	}
}


void
smb_export_dialog_prepare_new ()
{
	GtkCList *clist;

	clist = GTK_CLIST (my_get_widget ("smb_exports"));

	/* Set export name to none */

	gtk_entry_set_text (GTK_ENTRY (my_get_widget ("smb_export_name")), "");

	/* Set export path to none */

	gtk_entry_set_text (GTK_ENTRY (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (my_get_widget ("smb_export_path")))), "");

	/* Set export comment to none */

	gtk_entry_set_text (GTK_ENTRY (my_get_widget ("smb_export_comment")), "");

	/* Set export browseable boolean to off */

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (my_get_widget ("smb_export_browseable")), FALSE);

	/* Set export public boolean to off */

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (my_get_widget ("smb_export_public")), FALSE);

	/* Set export writable boolean to off */

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (my_get_widget ("smb_export_writable")), FALSE);
}


void
smb_export_dialog_affect_new ()
{
	GtkCList *clist;
	gchar *text;
	gint row;
	gchar *rowtext[32];
	xmlNodePtr node;
	int i;

	clist = GTK_CLIST (my_get_widget ("smb_exports"));

	/* Create top-level XML node */

	node = xml_element_add (xml_doc_get_root (root), "export");
	if (!node)
		return;		/* Should never happen */

	xml_element_set_attribute (node, "type", "smb");

	/* Add new row to end of list */

	for (i = 0; i < 32; i++)
		rowtext[i] = NULL;
	row = gtk_clist_append (clist, rowtext);

	/* Set export name in clist */

	text = gtk_entry_get_text (GTK_ENTRY (my_get_widget ("smb_export_name")));
	gtk_clist_set_pixtext (clist, row, 0, text, 0, folder_pixmap, folder_mask);

	/* Set export path in clist */

	text = path_nice (strdup (gtk_entry_get_text (GTK_ENTRY (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (my_get_widget ("smb_export_path")))))));
	gtk_clist_set_text (clist, row, 1, text);
	free (text);

	/* Set export comment in clist */

	text = gtk_entry_get_text (GTK_ENTRY (my_get_widget ("smb_export_comment")));
	gtk_clist_set_text (clist, row, 6, text);

	/* Set export enabled boolean in clist */

	set_clist_checkmark (clist, row, 2, TRUE);

	/* Set export browseable boolean in clist */

	set_clist_checkmark (clist, row, 3, gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (my_get_widget ("smb_export_browseable"))));

	/* Set export public boolean in clist */

	set_clist_checkmark (clist, row, 4, gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (my_get_widget ("smb_export_public"))));

	/* Set export writable boolean in clist */

	set_clist_checkmark (clist, row, 5, gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (my_get_widget ("smb_export_writable"))));

	/* Assign XML node to clist row */

	gtk_clist_set_row_data (clist, row, node);
}


void
on_smb_export_add (GtkButton * button, gpointer data)
{
	GnomeDialog *dialog;

	smb_export_dialog_prepare_new ();
	dialog = GNOME_DIALOG (my_get_widget ("smb_export_settings_dialog"));

	switch (gnome_dialog_run_and_close (dialog))
	{
		case 0:
			smb_export_dialog_affect_new ();
			break;

		case 1:
		case -1:
		default:
			break;
	}
}


void
on_smb_export_remove (GtkButton * button, gpointer data)
{
	GtkCList *clist;
	xmlNodePtr node;

	g_return_if_fail (smb_exports_row_selected != -1);

	clist = GTK_CLIST (my_get_widget ("smb_exports"));
	node = gtk_clist_get_row_data (clist, smb_exports_row_selected);
	gtk_clist_remove (clist, smb_exports_row_selected);
	xml_element_destroy (node);
}


void
smb_export_dialog_prepare_from_selected ()
{
	GtkCList *clist;
	gchar *text;
	guint8 spacing;
	GdkPixmap *pixmap;
	GdkBitmap *mask;

	clist = GTK_CLIST (my_get_widget ("smb_exports"));

	/* Get export name from clist */

	if (gtk_clist_get_pixtext (clist, smb_exports_row_selected, 0, &text, &spacing, &pixmap, &mask))
		gtk_entry_set_text (GTK_ENTRY (my_get_widget ("smb_export_name")), text);

	/* Get export path from clist */

	if (gtk_clist_get_text (clist, smb_exports_row_selected, 1, &text))
		gtk_entry_set_text (GTK_ENTRY (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (my_get_widget ("smb_export_path")))), text);

	/* Get export comment from clist */

	if (gtk_clist_get_text (clist, smb_exports_row_selected, 6, &text))
		gtk_entry_set_text (GTK_ENTRY (my_get_widget ("smb_export_comment")), text);

	/* Get export browseable boolean from clist */

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (my_get_widget ("smb_export_browseable")), get_clist_checkmark (clist, smb_exports_row_selected, 3));

	/* Get export public boolean from clist */

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (my_get_widget ("smb_export_public")), get_clist_checkmark (clist, smb_exports_row_selected, 4));

	/* Get export writable boolean from clist */

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (my_get_widget ("smb_export_writable")), get_clist_checkmark (clist, smb_exports_row_selected, 5));
}


void
smb_export_dialog_affect_selected ()
{
	GtkCList *clist;
	gchar *text;

	clist = GTK_CLIST (my_get_widget ("smb_exports"));

	/* Set export name in clist */

	text = gtk_entry_get_text (GTK_ENTRY (my_get_widget ("smb_export_name")));
	gtk_clist_set_pixtext (clist, smb_exports_row_selected, 0, text, 0, folder_pixmap, folder_mask);

	/* Set export path in clist */

	text = path_nice (strdup (gtk_entry_get_text (GTK_ENTRY (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (my_get_widget ("smb_export_path")))))));
	gtk_clist_set_text (clist, smb_exports_row_selected, 1, text);
	free (text);

	/* Set export comment in clist */

	text = gtk_entry_get_text (GTK_ENTRY (my_get_widget ("smb_export_comment")));
	gtk_clist_set_text (clist, smb_exports_row_selected, 6, text);

	/* Set export browseable boolean in clist */

	set_clist_checkmark (clist, smb_exports_row_selected, 3, gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (my_get_widget ("smb_export_browseable"))));

	/* Set export public boolean in clist */

	set_clist_checkmark (clist, smb_exports_row_selected, 4, gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (my_get_widget ("smb_export_public"))));

	/* Set export writable boolean in clist */

	set_clist_checkmark (clist, smb_exports_row_selected, 5, gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (my_get_widget ("smb_export_writable"))));
}


void
on_smb_export_settings (GtkButton * button, gpointer data)
{
	GnomeDialog *dialog;

	smb_export_dialog_prepare_from_selected ();
	dialog = GNOME_DIALOG (my_get_widget ("smb_export_settings_dialog"));

	switch (gnome_dialog_run_and_close (dialog))
	{
		case 0:
			smb_export_dialog_affect_selected ();
			break;

		case 1:
		case -1:
		default:
			break;
	}
}


void
nfs_export_dialog_prepare_new ()
{
	/* Set local path to none */

	gtk_editable_delete_text (GTK_EDITABLE (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (my_get_widget ("nfs_export_path")))), 0, -1);
}


void
nfs_export_dialog_affect_new ()
{
	xmlNodePtr node = NULL;
	GtkCTree *ctree;
	GtkCTreeNode *ctn;
	gboolean is_leaf;
	gchar *text, *s0;
	gchar *textrow[32];
	int i;

	ctree = GTK_CTREE (my_get_widget ("nfs_exports"));

	/* Find or make a suitable path node to attach to */

	s0 = path_nice (strdup (gtk_entry_get_text (GTK_ENTRY (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (my_get_widget ("nfs_export_path")))))));

	/* First, see if the target path is already in our tree */

	for (ctn = gtk_ctree_node_nth (ctree, 0); ctn; ctn = GTK_CTREE_NODE_NEXT (ctn))
	{
		gtk_ctree_get_node_info (ctree, ctn, NULL, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);
		if (is_leaf)
			continue;	/* Skip client nodes */

		gtk_ctree_get_node_info (ctree, ctn, &text, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
		if (text && !strcasecmp (text, s0))
			break;	/* FIXME: Display error msg */
	}

	for (i = 0; i < 32; i++)
		textrow[i] = NULL;

	textrow[0] = s0;
	ctn = gtk_ctree_insert_node (ctree, NULL, NULL, textrow, 0, folder_pixmap, folder_mask, folder_pixmap, folder_mask, FALSE, TRUE);

	gtk_ctree_node_set_selectable (ctree, ctn, TRUE);

	/* Create XML node */

	node = xml_element_add (xml_doc_get_root (root), "export");
	if (!node)
		return;		/* Should never happen */
	xml_element_set_attribute (node, "type", "nfs");

	/* Attach to ctree node */

	gtk_ctree_node_set_row_data (ctree, ctn, node);

	free (textrow[0]);
}


void
on_nfs_export_add (GtkButton * button, gpointer data)
{
	GnomeDialog *dialog;

	nfs_export_dialog_prepare_new ();
	dialog = GNOME_DIALOG (my_get_widget ("nfs_export_settings_dialog"));

	switch (gnome_dialog_run_and_close (dialog))
	{
		case 0:
			nfs_export_dialog_affect_new ();
			break;

		case 1:
		case -1:
		default:
			break;
	}
}


void
on_nfs_export_remove (GtkButton * button, gpointer data)
{
	GtkCTree *ctree;
	GtkCTreeNode *ctn;
	xmlNodePtr node;
	gboolean is_leaf;

	g_return_if_fail (nfs_exports_node_selected != NULL);

	ctree = GTK_CTREE (my_get_widget ("nfs_exports"));

	for (ctn = nfs_exports_node_selected; ctn; ctn = GTK_CTREE_NODE_PREV (ctn))
	{
		gtk_ctree_get_node_info (ctree, ctn, NULL, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);
		if (!is_leaf)
			break;	/* Found actual export root node */
	}

	node = gtk_ctree_node_get_row_data (ctree, ctn);
	gtk_ctree_remove_node (ctree, ctn);
	xml_element_destroy (node);

	nfs_exports_node_selected = NULL;

	gtk_widget_set_sensitive (my_get_widget ("nfs_export_remove"), FALSE);
	gtk_widget_set_sensitive (my_get_widget ("nfs_export_settings"), FALSE);
	gtk_widget_set_sensitive (my_get_widget ("nfs_client_add"), FALSE);
	gtk_widget_set_sensitive (my_get_widget ("nfs_client_remove"), FALSE);
	gtk_widget_set_sensitive (my_get_widget ("nfs_client_settings"), FALSE);
}


void
nfs_export_dialog_prepare_from_selected ()
{
	GtkCTree *ctree;
	GtkCTreeNode *ctn;
	gboolean is_leaf;
	gchar *text;

	g_return_if_fail (nfs_exports_node_selected != NULL);

	ctree = GTK_CTREE (my_get_widget ("nfs_exports"));

	for (ctn = nfs_exports_node_selected; ctn; ctn = GTK_CTREE_NODE_PREV (ctn))
	{
		gtk_ctree_get_node_info (ctree, ctn, NULL, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);
		if (!is_leaf)
			break;	/* Found actual export root node */
	}

	gtk_ctree_get_node_info (ctree, ctn, &text, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

	/* Set local path from ctree */

	if (text)
		gtk_entry_set_text (GTK_ENTRY (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (GNOME_FILE_ENTRY (my_get_widget ("nfs_export_path"))))), text);
	else
		gtk_editable_delete_text (GTK_EDITABLE (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (GNOME_FILE_ENTRY (my_get_widget ("nfs_export_path"))))), 0, -1);
}


void
nfs_export_dialog_affect_selected ()
{
	GtkCTree *ctree;
	GtkCTreeNode *ctn;
	gboolean is_leaf;
	gchar *text;

	g_return_if_fail (nfs_exports_node_selected != NULL);

	ctree = GTK_CTREE (my_get_widget ("nfs_exports"));

	for (ctn = nfs_exports_node_selected; ctn; ctn = GTK_CTREE_NODE_PREV (ctn))
	{
		gtk_ctree_get_node_info (ctree, ctn, NULL, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);
		if (!is_leaf)
			break;	/* Found actual export root node */
	}

	/* Set path in ctree */

	text = path_nice (strdup (gtk_entry_get_text (GTK_ENTRY (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (GNOME_FILE_ENTRY (my_get_widget ("nfs_export_path"))))))));
	gtk_ctree_node_set_pixtext (ctree, ctn, 0, text, 0, folder_pixmap, folder_mask);
	free (text);
}


void
on_nfs_export_settings (GtkButton * button, gpointer data)
{
	GnomeDialog *dialog;

	nfs_export_dialog_prepare_from_selected ();
	dialog = GNOME_DIALOG (my_get_widget ("nfs_export_settings_dialog"));

	switch (gnome_dialog_run_and_close (dialog))
	{
		case 0:
			nfs_export_dialog_affect_selected ();
			break;

		case 1:
		case -1:
		default:
			break;
	}
}


void
nfs_client_dialog_prepare_new ()
{
	/* Set client pattern to none */

	gtk_editable_delete_text (GTK_EDITABLE (my_get_widget ("nfs_client_pattern")), 0, -1);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (my_get_widget ("nfs_client_write_toggle")), FALSE);
}


void
nfs_client_dialog_affect_new ()
{
	xmlNodePtr node = NULL;
	GtkCTree *ctree;
	GtkCTreeNode *ctn;
	gboolean is_leaf;
	gchar *textrow[32];
	int i;

	ctree = GTK_CTREE (my_get_widget ("nfs_exports"));

	/* Find parent node */

	for (ctn = nfs_exports_node_selected; ctn; ctn = GTK_CTREE_NODE_PREV (ctn))
	{
		gtk_ctree_get_node_info (ctree, ctn, NULL, NULL, NULL, NULL, NULL, NULL, &is_leaf, NULL);
		if (!is_leaf)
			break;	/* Found actual export root node */
	}

	/* Create XML subnode with allowable pattern and write access */

	node = gtk_ctree_node_get_row_data (ctree, ctn);
	node = xml_element_add (node, "allow");
	xml_element_set_state (node, "write", gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (my_get_widget ("nfs_client_write_toggle"))));
	xml_element_add_with_content (node, "pattern", gtk_entry_get_text (GTK_ENTRY (my_get_widget ("nfs_client_pattern"))));

	for (i = 0; i < 32; i++)
		textrow[i] = NULL;

	/* Create pattern node */

	textrow[0] = gtk_entry_get_text (GTK_ENTRY (my_get_widget ("nfs_client_pattern")));
	ctn = gtk_ctree_insert_node (ctree, ctn, NULL, textrow, 0, computer_pixmap, computer_mask, computer_pixmap, computer_mask, TRUE, TRUE);

	set_ctree_checkmark (ctree, ctn, 1, gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (my_get_widget ("nfs_client_write_toggle"))));
	gtk_ctree_node_set_selectable (ctree, ctn, TRUE);
	gtk_ctree_node_set_row_data (ctree, ctn, node);
}


void
on_nfs_client_add (GtkButton * button, gpointer data)
{
	GnomeDialog *dialog;

	nfs_client_dialog_prepare_new ();
	dialog = GNOME_DIALOG (my_get_widget ("nfs_client_settings_dialog"));

	switch (gnome_dialog_run_and_close (dialog))
	{
		case 0:
			nfs_client_dialog_affect_new ();
			break;

		case 1:
		case -1:
		default:
			break;
	}
}


void
on_nfs_client_remove (GtkButton * button, gpointer data)
{
	GtkCTree *ctree;
	xmlNodePtr node;

	g_return_if_fail (nfs_exports_node_selected != NULL);

	ctree = GTK_CTREE (my_get_widget ("nfs_exports"));

	node = gtk_ctree_node_get_row_data (ctree, nfs_exports_node_selected);
	gtk_ctree_remove_node (ctree, nfs_exports_node_selected);
	xml_element_destroy (node);

	nfs_exports_node_selected = NULL;

	gtk_widget_set_sensitive (my_get_widget ("nfs_export_remove"), FALSE);
	gtk_widget_set_sensitive (my_get_widget ("nfs_export_settings"), FALSE);
	gtk_widget_set_sensitive (my_get_widget ("nfs_client_add"), FALSE);
	gtk_widget_set_sensitive (my_get_widget ("nfs_client_remove"), FALSE);
	gtk_widget_set_sensitive (my_get_widget ("nfs_client_settings"), FALSE);
}


void
nfs_client_dialog_prepare_from_selected ()
{
	GtkCTree *ctree;
	gchar *text;

	ctree = GTK_CTREE (my_get_widget ("nfs_exports"));

	/* Set client pattern to none */

	gtk_ctree_get_node_info (ctree, nfs_exports_node_selected, &text, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
	if (text)
		gtk_entry_set_text (GTK_ENTRY (my_get_widget ("nfs_client_pattern")), text);
	else
		gtk_editable_delete_text (GTK_EDITABLE (my_get_widget ("nfs_client_pattern")), 0, -1);

	/* Set allow write toggle to off */

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (my_get_widget ("nfs_client_write_toggle")), get_ctree_checkmark (ctree, nfs_exports_node_selected, 1));
}


void
nfs_client_dialog_affect_selected ()
{
	GtkCTree *ctree;
	gchar *text;

	g_return_if_fail (nfs_exports_node_selected != NULL);

	ctree = GTK_CTREE (my_get_widget ("nfs_exports"));

	/* Set client pattern in ctree */

	text = gtk_entry_get_text (GTK_ENTRY (my_get_widget ("nfs_client_pattern")));
	gtk_ctree_node_set_pixtext (ctree, nfs_exports_node_selected, 0, text, 0, computer_pixmap, computer_mask);

	/* Set allow write checkmark in ctree */

	set_ctree_checkmark (ctree, nfs_exports_node_selected, 1, gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (my_get_widget ("nfs_client_write_toggle"))));
}


void
on_nfs_client_settings (GtkButton * button, gpointer data)
{
	GnomeDialog *dialog;

	nfs_client_dialog_prepare_from_selected ();
	dialog = GNOME_DIALOG (my_get_widget ("nfs_client_settings_dialog"));

	switch (gnome_dialog_run_and_close (dialog))
	{
		case 0:
			nfs_client_dialog_affect_selected ();
			break;

		case 1:
		case -1:
		default:
			break;
	}
}
