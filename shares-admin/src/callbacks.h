#include <gnome.h>
#include <gnome-xml/tree.h>


extern xmlDocPtr doc;


void
on_imports_tree_select_row          (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_imports_tree_unselect_row        (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void on_listed_toggled (GtkToggleButton *toggle, gpointer data);
void on_mounted_toggled (GtkToggleButton *toggle, gpointer data);

void on_help_clicked(GtkButton *button, gpointer data);
void on_ok_clicked(GtkButton *button, gpointer data);
void on_apply_clicked(GtkButton *button, gpointer data);
void on_cancel_clicked(GtkButton *button, gpointer data);

void on_import_settings_launched(GtkButton *button, gpointer data);
void on_import_new_launched(GtkButton *button, gpointer data);

void on_import_type_selected(GtkMenuShell *menushell, gpointer user_data);

void
on_smb_exports_list_select_row          (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_smb_exports_list_unselect_row        (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void on_smb_export_enabled_toggled(GtkToggleButton *toggle, gpointer data);
void on_smb_export_add(GtkButton *button, gpointer data);
void on_smb_export_remove(GtkButton *button, gpointer data);
void on_smb_export_settings(GtkButton *button, gpointer data);

void
on_nfs_exports_tree_select_row         (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_nfs_exports_tree_unselect_row       (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void on_nfs_export_add(GtkButton *button, gpointer data);
void on_nfs_export_remove(GtkButton *button, gpointer data);
void on_nfs_export_settings(GtkButton *button, gpointer data);
void on_nfs_client_add(GtkButton *button, gpointer data);
void on_nfs_client_remove(GtkButton *button, gpointer data);
void on_nfs_client_settings(GtkButton *button, gpointer data);
