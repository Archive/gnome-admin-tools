#include <gnome.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

extern GdkPixmap *computer_pixmap, *folder_pixmap;
extern GdkBitmap *computer_mask, *folder_mask;

void pixmaps_init(void);
