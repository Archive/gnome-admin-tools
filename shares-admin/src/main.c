#include "config.h"

#include <stdio.h>

#include <gtk/gtk.h>
#include "interface-helpers.h"
#include "callbacks.h"
#include "xml.h"
#include "transfer.h"
#include "pixmaps.h"

#include <glade/glade.h>
#include <gnome.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gdk-pixbuf/gnome-canvas-pixbuf.h>

#include "reading.xpm"


xmlDocPtr root;


void delete_event (GtkWidget * widget, GdkEvent * event, gpointer gdata);
void main_connect_callbacks (void);
void read_glade_interface (gchar * interface_name);


void
delete_event (GtkWidget * widget, GdkEvent * event, gpointer gdata)
{
	gtk_main_quit ();
}


static void
handle_events_immediately ()
{
	while (gtk_events_pending ())
		gtk_main_iteration ();

	usleep (100000);

	while (gtk_events_pending ())
		gtk_main_iteration ();
}


void
main_connect_callbacks ()
{
	GtkWidget *w0;

	/* --- Toplevel --- */

	/* App delete */

	w0 = my_get_widget ("shares-admin");
	gtk_signal_connect (GTK_OBJECT (w0), "delete_event", delete_event, NULL);

	/* Help button */

	w0 = my_get_widget ("help");
	gtk_signal_connect (GTK_OBJECT (w0), "clicked", on_help_clicked, NULL);

	/* Ok button */

	w0 = my_get_widget ("ok");
	gtk_signal_connect (GTK_OBJECT (w0), "clicked", on_ok_clicked, NULL);

	/* Apply button */

	w0 = my_get_widget ("apply");
	gtk_signal_connect (GTK_OBJECT (w0), "clicked", on_apply_clicked, NULL);

	/* Cancel button */

	w0 = my_get_widget ("cancel");
	gtk_signal_connect (GTK_OBJECT (w0), "clicked", on_cancel_clicked, NULL);

	/* --- Imports tab --- */

	/* Imports tree selection */

	w0 = my_get_widget ("imports");
	gtk_signal_connect (GTK_OBJECT (w0), "select_row", on_imports_tree_select_row, NULL);
	gtk_signal_connect (GTK_OBJECT (w0), "unselect_row", on_imports_tree_unselect_row, NULL);

	/* Listed toggle */

	w0 = my_get_widget ("listed_toggle");
	gtk_signal_connect (GTK_OBJECT (w0), "toggled", on_listed_toggled, NULL);

	/* Mounted toggle */

	w0 = my_get_widget ("mounted_toggle");
	gtk_signal_connect (GTK_OBJECT (w0), "toggled", on_mounted_toggled, NULL);

	/* Settings button */

	w0 = my_get_widget ("import_settings_launch");
	gtk_signal_connect (GTK_OBJECT (w0), "clicked", on_import_settings_launched, NULL);

	/* New import button */

	w0 = my_get_widget ("import_new_launch");
	gtk_signal_connect (GTK_OBJECT (w0), "clicked", on_import_new_launched, NULL);

	/* --- Import settings window --- */

	w0 = gtk_option_menu_get_menu (GTK_OPTION_MENU (my_get_widget ("import_type")));
	gtk_signal_connect (GTK_OBJECT (w0), "selection-done", on_import_type_selected, NULL);

	/* --- SMB exports tab --- */

	/* Exports list selection */

	w0 = my_get_widget ("smb_exports");
	gtk_signal_connect (GTK_OBJECT (w0), "select_row", on_smb_exports_list_select_row, NULL);
	gtk_signal_connect (GTK_OBJECT (w0), "unselect_row", on_smb_exports_list_unselect_row, NULL);

	/* Enabled toggle */

	w0 = my_get_widget ("smb_export_enabled");
	gtk_signal_connect (GTK_OBJECT (w0), "toggled", on_smb_export_enabled_toggled, NULL);

	/* New export button */

	w0 = my_get_widget ("smb_export_add");
	gtk_signal_connect (GTK_OBJECT (w0), "clicked", on_smb_export_add, NULL);

	/* Remove export button */

	w0 = my_get_widget ("smb_export_remove");
	gtk_signal_connect (GTK_OBJECT (w0), "clicked", on_smb_export_remove, NULL);

	/* Export settings button */

	w0 = my_get_widget ("smb_export_settings");
	gtk_signal_connect (GTK_OBJECT (w0), "clicked", on_smb_export_settings, NULL);

	/* --- NFS exports tab --- */

	/* Exports list selection */

	w0 = my_get_widget ("nfs_exports");
	gtk_signal_connect (GTK_OBJECT (w0), "select_row", on_nfs_exports_tree_select_row, NULL);
	gtk_signal_connect (GTK_OBJECT (w0), "unselect_row", on_nfs_exports_tree_unselect_row, NULL);

	/* Add export button */

	w0 = my_get_widget ("nfs_export_add");
	gtk_signal_connect (GTK_OBJECT (w0), "clicked", on_nfs_export_add, NULL);

	/* Remove export button */

	w0 = my_get_widget ("nfs_export_remove");
	gtk_signal_connect (GTK_OBJECT (w0), "clicked", on_nfs_export_remove, NULL);

	/* Export settings button */

	w0 = my_get_widget ("nfs_export_settings");
	gtk_signal_connect (GTK_OBJECT (w0), "clicked", on_nfs_export_settings, NULL);

	/* Add client button */

	w0 = my_get_widget ("nfs_client_add");
	gtk_signal_connect (GTK_OBJECT (w0), "clicked", on_nfs_client_add, NULL);

	/* Remove client button */

	w0 = my_get_widget ("nfs_client_remove");
	gtk_signal_connect (GTK_OBJECT (w0), "clicked", on_nfs_client_remove, NULL);

	/* Client settings button */

	w0 = my_get_widget ("nfs_client_settings");
	gtk_signal_connect (GTK_OBJECT (w0), "clicked", on_nfs_client_settings, NULL);
}


void
read_glade_interface (gchar * interface_name)
{
	gchar *path;

	path = g_strjoin ("/", SCRIPT_DIR, interface_name, NULL);

	glade_interface = glade_xml_new (path, NULL);
	if (!glade_interface)
		g_error ("Can't find glade interface file.");
}


int reply;

static void
reply_cb (gint val, gpointer data)
{
	reply = val;
	gtk_main_quit ();
}


int
main (int argc, char *argv[])
{
	GtkWidget *w0, *reading, *pixmap;

	gnome_init ("shares-admin", "0.1", argc, argv);
	glade_gnome_init ();
	pixmaps_init ();

	if (geteuid () != 0)
	{
		gnome_ok_cancel_dialog (_("You need full administration privileges (i.e. root)\n"
                              "to run this configuration tool. You can acquire\n"
                              "such privileges by issuing an \"su\" command in a shell.\n\n"
                              "Continue anyway?"), reply_cb, NULL);

		gtk_main ();

		if (reply) exit(1);
	}

	read_glade_interface ("shares-admin.glade");
  
	pixmap = gnome_pixmap_new_from_xpm_d(reading_xpm);
	gtk_box_pack_end(GTK_BOX(my_get_widget("reading_box")),
                   pixmap, TRUE, TRUE, 0);
	reading = my_get_widget("reading");
	gtk_widget_show(pixmap);
	gtk_widget_show(reading);
  
	handle_events_immediately();

	root = xml_doc_read_from_backend ("shares-conf");
	if (!root)
		g_error ("Unable to read from backend.");

	transfer_xml_to_gui (xml_doc_get_root (root));
	gtk_widget_hide(reading);

	main_connect_callbacks ();

	w0 = glade_xml_get_widget (glade_interface, "imports");
	gtk_ctree_set_indent (GTK_CTREE (w0), 16);

	w0 = my_get_widget ("shares-admin");
	gtk_widget_show (w0);

	gtk_main ();
	return 0;
}
