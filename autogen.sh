#!/bin/sh
# Run this to generate all the initial makefiles, etc.

tools=`cat Packages`
toplevel=`pwd`

for i in $tools
do
  echo
  echo
  echo \|
  echo \| Running autogen for $i.
  echo \|
  echo
  echo

  cp -Rf macros intl $i
  cd $i
  ./autogen.sh $*
  cd $toplevel
done
