#!/bin/sh
# Run this to build packages.

tools=`cat Packages`
toplevel=`pwd`

for i in $tools
do
  echo
  echo
  echo \|
  echo \| Building tarball from $i.
  echo \|
  echo
  echo

  cd $i
  make dist
  cd $toplevel
  mv -f $i/$i-*.tar.gz .
done
